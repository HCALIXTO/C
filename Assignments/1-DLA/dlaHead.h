#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "neillncurses.h"
#define ROWS 50
#define COLS 50
#define NPARTICLES 250
#define OCCUPIED '#'
#define FREE ' '

enum sides {bottom, left, top, right};

typedef enum sides sides;

struct particle{
   int x;
   int y;
   int colision;/*0-no colision; 1-colision*/
};

typedef struct particle particle;

int delimitedRandom(int min, int max);

void setGrid(char grid[ROWS][COLS]);

void printGrid(char grid[ROWS][COLS]);

void toroidalEnforce(particle *part);

void setParticle(particle *part, char grid[ROWS][COLS]);

void moveParticle(particle *part);

int checkColision(particle *part, char grid[ROWS][COLS]);

void setNeighbourParticle(particle *part, particle *neig, sides side);

