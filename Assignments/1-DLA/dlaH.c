#include "dlaHead.h"

int main(void){
   char grid[ROWS][COLS];
   int pCount = 0;
   particle p;
   NCURS_Simplewin sw;/*Here where all the display things are*/
   
   srand(time(NULL));/*seed the random with time, must do once in main*/
   setGrid(grid);/*set the grid*/
   /* Initialize the display and define the collor scheme*/
   Neill_NCURS_Init(&sw);
   Neill_NCURS_CharStyle(&sw, "#", COLOR_RED, COLOR_RED, A_NORMAL);
   Neill_NCURS_CharStyle(&sw, " ", COLOR_BLACK, COLOR_BLACK, A_NORMAL);
   
   /*Crete the particles*/
   while(pCount < NPARTICLES){
      /*Set the particles and move it until it colide*/
      setParticle(&p, grid);
      while(checkColision(&p, grid)){
         moveParticle(&p);
      }
      /*Register the place it colided on the GRID*/
      grid[p.y][p.x] = OCCUPIED;
      Neill_NCURS_PrintArray(&grid[0][0], ROWS, COLS, &sw);
      Neill_NCURS_Delay(30.0);
      pCount++;
   }
   /*To keep showing after it finishes*/
   do{
      Neill_NCURS_PrintArray(&grid[0][0], ROWS, COLS, &sw);
      Neill_NCURS_Delay(10.0);
      Neill_NCURS_Events(&sw);
   }while(!sw.finished);
   /* Call this function if we exit() anywhere in the code */
   atexit(Neill_NCURS_Done);
   exit(EXIT_SUCCESS);
   return 0;
}

void setParticle(particle *part, char grid[ROWS][COLS]){
   /*Set a inicial particle in one of the corners,
   with the colision set to zero*/
   int side = delimitedRandom(0, 3);/*To chose one side*/
   part->colision = 0;
   /*Repeat until I find a free spot*/
   do{
      switch(side){
         case bottom:
            part->x = delimitedRandom(0, COLS);
            part->y = 0;
            break;
         case left:
            part->x = 0;
            part->y = delimitedRandom(0, ROWS);
            break;
         case top:
            part->x = delimitedRandom(0, COLS);
            part->y = ROWS;
            break;
         case right:
            part->x = COLS;
            part->y = delimitedRandom(0, ROWS);
            break;
         default:/*In case something weird happen*/
            part->x = 0;
            part->y = 0;
            break;
      }
   }while(grid[part->y][part->x] == OCCUPIED);
}

void setNeighbourParticle(particle *part,\
       particle *neig, sides side){
   /*Set a neighbour particle according to a reference particle*/
   switch(side){
      case bottom:
         neig->x = part->x-1;
         neig->y = part->y;
         break;
      case left:
         neig->x = part->x;
         neig->y = part->y-1;
         break;
      case top:
         neig->x = part->x+1;
         neig->y = part->y;
         break;
      case right:
         neig->x = part->x;
         neig->y = part->y+1;
         break; 
      default:/*In case something weird happen*/
         neig->x = 0;
         neig->y = 0;
         break;
   }
   toroidalEnforce(neig);
}

void moveParticle(particle *part){
   /*Move the particle in the X or Y direction and
   enforce the toroidal behaviour*/
   int axis = delimitedRandom(0, 1);/*To chose one axis*/
   /*if direction is 0 the movement is positive*/
   int direction = delimitedRandom(0, 1);
   switch(axis){
      case 0:/* move in the x axis*/
         part->x += (1-direction)-(1*direction);
         break;
      case 1:/*move in the y axis*/
      default:/*In case something weird happen*/
         part->y += (1-direction)-(1*direction);
         break;
   }
   /*Toroidal enforcing*/
   toroidalEnforce(part);
}

int checkColision(particle *part, char grid[ROWS][COLS]){
   /*Set one neighbour for each side
   and check if it is occupied*/
   particle neighbour;
   sides side;
   
   for(side = bottom; side <= right; side++){
      setNeighbourParticle(part, &neighbour, side);
      if(grid[neighbour.y][neighbour.x] == OCCUPIED){
         return 0;/*Return false beacause we colided*/
      }
   }
   /*There is no colision, return true to keep going*/
   return 1;
}

void toroidalEnforce(particle *part){
   /*enforcing toroidal behaviour*/
   if(part->x < 0){
      part->x = COLS;
   }
   if(part->x > COLS){
      part->x = 0;
   }
   if(part->y < 0){
      part->y = ROWS;
   }
   if(part->y > ROWS){
      part->y = 0;
   }
}

void setGrid(char grid[ROWS][COLS]){
   int r, c;
   /*Set all the cells in the grid to free*/
   for(r = 0; r<ROWS ; r++){
      for(c=0; c<COLS ; c++){
         grid[r][c] = FREE;
      }
   }
   /*Set the cell in the middle as occupied (seed cell)*/
   grid[ROWS/2][COLS/2] = OCCUPIED;
}

void printGrid(char grid[ROWS][COLS]){
   /*Print the grid as text on the terminal*/
   int r, c;
   for(r = 0; r < ROWS; r++){
      for(c = 0; c < COLS; c++){
         printf("%c", grid[r][c]);
      }
      printf("\n");
   }
}

int delimitedRandom(int min, int max){
   return (rand()%(max-min+1))+min;
}
