#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "set.h"
#define INITSETSIZE 0
#define FIRSTPOS 0
#define EQUAL 0
#define FALSE 0
#define TRUE 1
#define NOTFOUND -1


/*Internal functions prototype*/
int find_element(set* s, arrtype l);

void pop_from_position(set* s, int p);

arrtype get_from_position(set* s, int p);

void copy_into_set(set* src, set* dst);

/*External functions*/
/* Create empty set */
set* set_init(void){
   set* s;

   s = calloc(1,sizeof(set));
   if(s == NULL){
      ON_ERROR("Creation of Set Failed\n");
   }
   s->ua = arr_init();
   s->sz = INITSETSIZE;
   return s;
}

/* Create new set, copied from another */
set* set_copy(set* s){
   set* newS;

   newS = set_init();
   copy_into_set(s, newS);
   return newS;
}

/* Create new set, copied from an array of length n*/
set* set_fromarray(arrtype* a, int n){
   set* newS;
   int pos;

   newS = set_init();
   for(pos=FIRSTPOS ;pos < n; pos++){
      set_insert(newS, *a);
      a++;
   }
   return newS;
}

/* Basic Operations */
/* Add one element into the set */
void set_insert(set* s, arrtype l){
   if(!set_contains(s, l) && s){
      arr_set(s->ua, s->sz, l);
      s->sz += 1;
   }
}

/* Return size of the set */
int set_size(set* s){
   if(s){
      return s->sz;
   }else{
      return INITSETSIZE;
   }
}

/* Returns true if l is in the array, false elsewise */
int set_contains(set* s, arrtype l){
   if(find_element( s, l) != NOTFOUND){
      return TRUE;
   }else{
      return FALSE;
   }
}

/* Remove l from the set (if it's in) */
void set_remove(set* s, arrtype l){
   pop_from_position(s, find_element(s, l));
}

/* Remove one element from the set - there's no
   particular order for the elements, so any will do */
arrtype set_removeone(set* s){
   arrtype el;

   if(set_size(s) > 0){
      el = get_from_position(s, FIRSTPOS);
      pop_from_position(s, FIRSTPOS);
      return el;
   }else{
      ON_ERROR("Can't remove from an empty set.\n");
   }
}

/* Operations on 2 sets */
/* Create a new set, containing all elements from s1 & s2 */
set* set_union(set* s1, set* s2){
   set* newS;

   newS = set_init();
   copy_into_set(s1, newS);
   copy_into_set(s2, newS);

   return newS;
}

/* Create a new set, containing all elements in both s1 & s2 */
set* set_intersection(set* s1, set* s2){
   set* newS;
   arrtype el;
   int pos;

   newS = set_init();
   for(pos = FIRSTPOS;pos < set_size(s1); pos++){
      el = get_from_position(s1, pos);
      if(set_contains(s2, el)){
         set_insert(newS, el);
      }
   }
   return newS;
}

/* Finish up */
/* Clears all space used, and sets pointer to NULL */
void set_free(set** s){
   if(s){
      set* st = *s;
      arr_free(&st->ua);
      free(st);
      /* Helps to assert that the job has been done.*/
      *s = NULL;
   }
}

/*Internal functions*/
/*Return the index of an element or -1 if it is not in the set*/
int find_element(set* s, arrtype l){
   int arrSize = set_size(s)-1, i;
   arrtype el;
   /*If s is NULL the loop won't run since arrSize would be -1*/
   for(i=FIRSTPOS; i <= arrSize; i++){
      el = get_from_position(s, i);
      /*memcmp to compare memory, isn't type dependant*/
      if(memcmp(&el, &l, sizeof(arrtype)) == EQUAL){
         return i;
      }
   }
   return NOTFOUND;
}

/*Pop a element from the desired position*/
void pop_from_position(set* s, int p){
   if(s){
      int arrSize = set_size(s)-1;
      /*if I`m in the set scope and if the set has anything*/
      if(p >= FIRSTPOS && p <= arrSize){
         for(; p < arrSize; p++){
            /*Set the arr[position] to arr[position+1]*/
            arr_set(s->ua, p, get_from_position(s, p+1));
         }
         s->sz -= 1;/*Reduce the size*/
      }
   }
}

/*Get a element from the desired position*/
arrtype get_from_position(set* s, int p){
   int arrSize = set_size(s)-1;
   /*if I`m in the set scope and if the set has anything*/
   if(s && p >= FIRSTPOS && p <= arrSize){
      return arr_get(s->ua, p);
   }else{
      ON_ERROR("Requested position/set is not allowed.\n");
   }
}

/*Copy the elements from source into destine set*/
void copy_into_set(set* src, set* dst){
   if(src && dst){
      int pos;

      for(pos = FIRSTPOS;pos < set_size(src); pos++){
         set_insert(dst, get_from_position(src, pos));
      }
   }
}
