#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bst.h"

#define DICT "eng_370k_shuffle.txt"
#define BOOK "heart_darkness.txt"
#define STRSIZE 40
#define MAXINPUT 50
#define NUMINPUTS 3
#define DICTINDEX 1
#define BOOKINDEX 2

void assign_arguments(int argc, char const *argv[],
                      char *dictName, char *bookName);

void put_file_into_bst(bst* b, bst* dict, char *fileName);

char* myprintstr(const void* v);

int mystrcmp(const void* a, const void* b);

int main(int argc, char const *argv[]){
   bst* dict;
   bst* misspelt;
   char dictName[MAXINPUT], bookName[MAXINPUT];

   assign_arguments(argc, argv, dictName, bookName);

   dict = bst_init(STRSIZE, mystrcmp, myprintstr);
   put_file_into_bst(dict, NULL, dictName);

   misspelt = bst_init(STRSIZE, mystrcmp, myprintstr);
   put_file_into_bst(misspelt, dict, bookName);

   bst_free(&dict);
   bst_free(&misspelt);

   return 0;
}

/*Reads and validates the arguments passed on argv, set it to
  default if none are given or the given ones are too long*/
void assign_arguments(int argc, char const *argv[],
                      char *dictName, char *bookName){
   if(argc == NUMINPUTS && 
      strlen(argv[DICTINDEX]) < MAXINPUT &&
      strlen(argv[BOOKINDEX]) < MAXINPUT){

         strcpy(dictName, argv[DICTINDEX]);
         strcpy(bookName, argv[BOOKINDEX]);

   }else{

      printf("You should run this program as: %s %s %s\n",
             argv[0], DICT, BOOK);
      printf("Using default values for dict and book\n");
      strcpy(dictName, DICT);
      strcpy(bookName, BOOK);

   }
}

/*Reads a file's words and put into a bst, if a dict is
  provided it include only the words not in the dict
  and print all the included words once*/
void put_file_into_bst(bst* b, bst* dict, char *fileName){
   FILE *f;
   char str[STRSIZE];

   f = fopen(fileName, "r");
   if(f == NULL){
      printf("Unable to open %s.\n", fileName);
      exit(1);
   }
   while(fscanf(f, "%s", str) > 0){
      if(dict){
         if(!bst_isin(dict, str)){/*Check if is not in the dict*/
            if(!bst_isin(b, str)){/*Check if is a new word*/
               printf("%s\n", str);/*print it*/
               bst_insert(b, str);/*include it*/
            }
         }
      }else{
         bst_insert(b, str);
      }
      
   }
}

char* myprintstr(const void* v){
   return (char*)v;
}

int mystrcmp(const void* a, const void* b){
   return strcmp(a, b);
}
