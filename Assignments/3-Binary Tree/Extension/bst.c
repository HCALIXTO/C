#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "bst.h"

#define EXTRCHAR 3
#define LEFT -1
#define RIGHT 1
#define NEW 0

/*Internal functions prototype*/

bstnode* node_init(int size, void* v, bstnode* parent);

void node_insert(bstnode** n, bstnode* parent,
                 void* v, bool balanced, bst* tree);

void node_delete(void* v, bool balanced, bst* tree);

bstnode* go_left(bstnode* n, bst* tree);

void remove_node(bstnode* n, bstnode* newN, bst* tree);

void updateNodeHeights(bstnode* n);

void rebalance(bstnode* n, int origin, bst* tree);

void rotateRight(bstnode* n, bst* tree, bstnode** nPos);

void rotateRightRight(bstnode* n, bst* tree, bstnode** nPos);

void rotateRightLeft(bstnode* n, bst* tree, bstnode** nPos);

void rotateLeft(bstnode* n, bst* tree, bstnode** nPos);

void rotateLeftLeft(bstnode* n, bst* tree, bstnode** nPos);

void rotateLeftRight(bstnode* n, bst* tree, bstnode** nPos);

void changeFatherSonParent(bstnode* father, bstnode* son, bst* tree);

int findNextDirection(bstnode* n);

int node_search(int(*comp)(const void* a, const void* b),
                bstnode* n, void* v);

bstnode* node_get(int(*comp)(const void* a, const void* b),
                bstnode* n, void* v);

int node_maxdepth(bstnode* n);

int node_count(bstnode* n);

void node_free(bstnode** n);

char* node_print(char*(*prnt)(const void* a), bstnode *n);

void node_traversal(bstnode *n, void **v, int size);

void bst_insertOrderedArr(bst* b, void* arr, int size, int start, int end);

/*Basic Functions*/
/*Initialize a binary search tree*/
bst* bst_init(int sz, bool sB,
              int(*comp)(const void* a, const void* b),
              char*(*prnt)(const void* a)  ){
   if(sz && comp && prnt){
      bst* tree = (bst*)calloc(1, sizeof(bst));
      if(tree){
         tree->top = NULL;
         tree->selfBalance = sB;
         tree->elsz = sz;
         tree->compare = comp;
         tree->prntnode = prnt;
         return tree;
      }else{
         ON_ERROR("Calloc faild on bst_init. Insuficient memory.");
      }
   }else{
      ON_ERROR("Not enough parameters for bst_init.");
   }
}

/* Insert 1 item into the tree */
void bst_insert(bst* b, void* v){
   if(b){
      node_insert(&b->top, NULL, v, b->selfBalance, b);
   }
}

void bst_delete(bst* b, void* v){
   if(b){
      node_delete(v, b->selfBalance, b);
   }
}

/* Number of nodes in tree */
int bst_size(bst* b){
   if(b){
      return node_count(b->top);
   }else{
      return 0;
   }
}

/* Longest path from root to any leaf */
int bst_maxdepth(bst* b){
   if(b){
      return node_maxdepth(b->top);
   }else{
      return 0;
   }
}

/* Whether the data in v, is stored in the tree */
bool bst_isin(bst* b, void* v){
   if(b && v){
      return node_search(b->compare, b->top, v);
   }else{
      return false;
   }
}

/* Bulk insert n items from an array v into an initialised tree */
void bst_insertarray(bst* b, void* v, int n){
   if(b && v && n){
      int i = 0;
      while(i < n){
         bst_insert(b, v);
         v = (void*)((char *)v + b->elsz);
         i++;
      }
   }
}

/* Clear all memory associated with tree, & set pointer to NULL */
void bst_free(bst** p){
   if(p){
      bst* tree = *p;
      node_free(&tree->top);
      free(*p);
      *p = NULL;
   }
}

/*Advanced functionality*/
/* Return a string displaying the tree in a textual form (head(left)(right)) recursively */
/* USER MUST FREE the returning str*/
char* bst_print(bst* b){
   if(b){
      return node_print(b->prntnode, b->top);
   }else{
      return node_print(NULL, NULL);
   }
}

/* Fill an array with a copy of the sorted tree data */
void bst_getordered(bst* b, void* v){
   if(b && v){
      node_traversal(b->top, &v, b->elsz);
   }
}

/* Rebalance the tree, recursively using the median of the sorted keys */
bst* bst_rebalance(bst* b){
   int nNodes = bst_size(b);
   int size = b->elsz;
   void* arr = calloc(nNodes, size);
   bst* newB = bst_init(b->elsz, false, b->compare, b->prntnode);
   bst_getordered(b, arr);
   bst_insertOrderedArr(newB, arr, size, 0, nNodes-1);
   free(arr);
   return newB;
}

/*Insert elements from a ordered arr in a way that the resulting tree is balanced*/
void bst_insertOrderedArr(bst* b, void* arr, int size, int start, int end){
   if(start <= end){
      int middle = (start+end)/2;
      void *midPoint = (void*)((char*)arr+(middle*size));/*a pointer to arr's middle*/
      /*printf("%d - %d - %d - %d \n", start, end, middle, *(int*)midPoint);*/
      bst_insert(b, midPoint);/*Inser the arr's middle value*/
      bst_insertOrderedArr(b, arr, size, start, middle-1);/*Pass the left arr*/
      bst_insertOrderedArr(b, arr, size, middle+1, end);/*Pass the right arr*/
   }
}

/*internal functions*/
/*initialize a binary tree node*/
bstnode* node_init(int size, void* v, bstnode* parent){
   bstnode* n = (bstnode*)calloc(1, sizeof(bstnode));
   void* data = calloc(1, size);
   if(n && data){
      n->data = memcpy(data, v, size);
      n->parent = parent;
      n->left = NULL;
      n->right = NULL;
      n->leftHeight = 0;
      n->rightHeight = 0;
      return n;
   }else{
      ON_ERROR("Calloc faild on node_init. Insuficient memory.");
   }
}

/*delete a node from a binary tree*/
void node_delete(void* v, bool balanced, bst* tree){
   bstnode* n = node_get(tree->compare, tree->top, v);
   if(n){
      if(n->left && n->right){
         void* data = calloc(1, tree->elsz);
         bstnode* leftMost =  go_left(n->right, tree);
         free(n->data);
         
         if(data){
            n->data = memcpy(data, leftMost->data, tree->elsz);
            node_free(&leftMost);
         }else{
            ON_ERROR("Calloc faild on node_delete. Insuficient memory.");
         }
      }else if(n->left){
         printf("Removing node with a left son\n");
         remove_node(n, n->left, tree);
      }else if(n->right){
         printf("Removing node with a right son\n");
         remove_node(n, n->right, tree);
      }else{
         printf("Removing node with NO son\n");
         remove_node(n, NULL, tree);
      }
      if(balanced){
         printf("Not yet finished\n");
      }
   }
}

void remove_node(bstnode* n, bstnode* newN, bst* tree){
   int direction = findNextDirection(n);

   if(direction == LEFT){
      n->parent->left = newN;
      if(newN){
         newN->parent = n->parent;
      }
   }else if(direction == RIGHT){
      n->parent->right = newN;
      if(newN){
         newN->parent = n->parent;
      }
   }else{/*If direction isnt left or right it is the tree top*/
      tree->top = newN;
      newN->parent = NULL;
   }

   node_free(&n);
}

/*Get the leftmost node for the node delete*/
bstnode* go_left(bstnode* n, bst* tree){
   if(n->left){
      return go_left(n->left, tree);
   }else{
      if(n->right){
         /*cant remove_node because I dont want to free it yet*/
         int direction = findNextDirection(n);
         if(direction == LEFT){
            n->parent->left = n->right;
            n->right->parent = n->parent;
         }else if(direction == RIGHT){
            n->parent->right = n->right;
            n->right->parent = n->parent;
         }else{/*If direction isnt left or right it is the tree top*/
            tree->top = n->right;
            n->right->parent = NULL;
         }
      }
      return n;
   }
}

/*insert a node in a binary tree*/
void node_insert(bstnode** n, bstnode* parent,
                 void* v, bool balanced, bst* tree){
   if(*n){
      bstnode* nd = *n;
      int dif = tree->compare(nd->data, v);
      if(dif != 0){
         if(dif < 0){/*Decide which side to go*/
            node_insert(&nd->right, *n, v, balanced, tree);
         }else{
            node_insert(&nd->left, *n, v, balanced, tree);
         }
      }
   }else{/*If we reach a leaf*/
      *n = node_init(tree->elsz, v, parent);
      if(balanced){
         bstnode* nd = *n;
         int direction = 0;/*direction is zero because it is a new node*/
         rebalance(nd, direction, tree);
      }
   }
}

/*Go bottom-up rebalancing a tree after a node insertion*/
void rebalance(bstnode* n, int origin, bst* tree){
   if(n){
      int direction = 0;
      int balancedFactor = 0;
      /*Increment the side's sizes*/
      if(origin == LEFT){
         n->leftHeight++;
      }else{/*origin can be 0 if it is a new node*/
         n->rightHeight += origin;
      }
      /*Debug prints
      printf("Data - %s \n", tree->prntnode(n->data));
      printf("LeftH - %d | RightH - %d \n", n->leftHeight, n->rightHeight);*/
      balancedFactor = n->rightHeight - n->leftHeight;
      if(balancedFactor > 1){
         rotateRight(n, tree, &n);
      }
      if(balancedFactor < -1){
         rotateLeft(n, tree, &n);
      }
      /*n can change during the rotation, so I need to check if Im not in the top*/
      if(n){
         direction = findNextDirection(n);
         rebalance(n->parent, direction, tree);
      }
   }
}

/*check the next level balancing factor to decide if is right-right or right-left*/
void rotateRight(bstnode* n, bst* tree, bstnode** nPos){
   int balancedFactor = n->right->rightHeight - n->right->leftHeight;
   if(balancedFactor > 0){
      rotateRightRight(n, tree, nPos);
   }else{
      rotateRightLeft(n, tree, nPos);
   }
}

/*check the next level balancing factor to decide if is left-left or left-right*/
void rotateLeft(bstnode* n, bst* tree, bstnode** nPos){
   int balancedFactor = n->left->rightHeight - n->left->leftHeight;
   if(balancedFactor < 0){
      rotateLeftLeft(n, tree, nPos);
   }else{
      rotateLeftRight(n, tree, nPos);
   }
}

/*Simple rotation to rearrange a right-right unbalanced scenario*/
void rotateRightRight(bstnode* n, bst* tree, bstnode** nPos){
   bstnode* father = n;
   bstnode* son = father->right;
   
   changeFatherSonParent(father, son, tree);
   /*Simple rotation*/
   father->right = son->left;
   if(father->right){
      father->right->parent = father;
   }
   son->left = father;
   /*Update the heights from tem bottom to top*/
   updateNodeHeights(father);
   updateNodeHeights(son);
   updateNodeHeights(son->parent);
   /*Here I change the node position to the rotation parent, so 
     I dont visit the same node twice*/
   *nPos = son->parent;
}

/*Double tree rotation to resolve a right-left scenario*/
void rotateRightLeft(bstnode* n, bst* tree, bstnode** nPos){
   bstnode* father = n;
   bstnode* son = father->right;
   bstnode* grSon = son->left;
   
   changeFatherSonParent(father, grSon, tree);
   /*Double rotation*/
   father->right = grSon->left;
   if(father->right){
      father->right->parent = father;
   }
   grSon->left = father;/*already update father's parent*/
   
   son->left = grSon->right;
   if(son->left){
      son->left->parent = son;
   }
   grSon->right = son;
   son->parent = grSon;
   /*Update the heights from tem bottom to top*/
   updateNodeHeights(father);
   updateNodeHeights(son);
   updateNodeHeights(grSon);
   updateNodeHeights(grSon->parent);
   /*Here I change the node position to the rotation parent, so 
     I dont visit the same node twice*/
   *nPos = grSon->parent;
}

/*Simple rotation to rearrange a left left unbalanced scenario*/
void rotateLeftLeft(bstnode* n, bst* tree, bstnode** nPos){
   bstnode* father = n;
   bstnode* son = n->left;
   
   changeFatherSonParent(father, son, tree);
   /*Simple rotation*/
   father->left = son->right;
   if(father->left){
      father->left->parent = father;
   }
   son->right = father;
   /*Update the heights from tem bottom to top*/
   updateNodeHeights(father);
   updateNodeHeights(son);
   updateNodeHeights(son->parent);
   /*Here I change the node position to the rotation parent, so 
     I dont visit the same node twice*/
   *nPos = son->parent;
}

/*Double tree rotation to resolve a left-right scenario*/
void rotateLeftRight(bstnode* n, bst* tree, bstnode** nPos){
   bstnode* father = n;
   bstnode* son = father->right;
   bstnode* grSon = son->left;
   
   changeFatherSonParent(father, grSon, tree);
   /*Double rotation*/
   father->left = grSon->right;
   if(father->left){
      father->left->parent = father;
   }
   grSon->right = father;/*already update father's parent*/
   
   son->right = grSon->left;
   if(son->right){
      son->right->parent = son;
   }
   grSon->left = son;
   son->parent = grSon;
   /*Update the heights from tem bottom to top*/
   updateNodeHeights(father);
   updateNodeHeights(son);
   updateNodeHeights(grSon);
   updateNodeHeights(grSon->parent);
   /*Here I change the node position to the rotation parent, so 
     I dont visit the same node twice*/
   *nPos = grSon->parent;
}

/*Update the height count based on the left and right nodes*/
void updateNodeHeights(bstnode* n){
   if(n){
      if(n->left){
         n->leftHeight = n->left->leftHeight > n->left->rightHeight ?
                         n->left->leftHeight + 1 : n->left->rightHeight + 1;
      }else{
         n->leftHeight = 0;
      }
      if(n->right){
         n->rightHeight = n->right->leftHeight > n->right->rightHeight ?
                         n->right->leftHeight + 1 : n->right->rightHeight + 1;
      }else{
         n->rightHeight = 0;
      }
   }
}

/*Switch two nodes parents, and update the parent left/right nodes*/
void changeFatherSonParent(bstnode* father, bstnode* son, bst* tree){
   son->parent = father->parent;
   father->parent = son;
   if(son->parent){
      bstnode* pr = son->parent;
      if(pr->left == father){
         pr->left = son;
      }else if(pr->right == father){
         pr->right = son;
      }
   }else{
      tree->top = son;
   }
}

/*Check if the node is a left or right son*/
int findNextDirection(bstnode* n){
   if(n){
      /*If parent is null I dont care with what I return, since it wont be used*/
      if(n->parent){
         if(n->parent->left == n){
            return LEFT;
         }
         if(n->parent->right == n){
            return RIGHT;
         }
      }
   }
   return NEW;
}

/*Return the number of nodes related to one note*/
int node_count(bstnode* n){
   int count = 0;
   if(n){
      count += 1;/*Count this node*/
      count += node_count(n->left);/*Count the nodes on the left*/
      count += node_count(n->right);/*Count the nodes on the right*/
   }
   return count;
}

/*Return the maxdepth related to one note*/
int node_maxdepth(bstnode* n){
   int count = 0;
   int leftDepth = 0;
   int rightDepth = 0;
   if(n){
      count += 1;/*Count this node*/
      leftDepth = node_maxdepth(n->left);/*Get the left max depth*/
      rightDepth = node_maxdepth(n->right);/*Get the right max depth*/
      count += (leftDepth > rightDepth) ? leftDepth : rightDepth;
   }
   return count;
}

/*Look at the nodes searching for v return True if found*/
int node_search(int(*comp)(const void* a, const void* b),
                bstnode* n, void* v){
   if(n){
      int dif = comp(n->data, v);
      if(dif != 0){
         if(dif < 0){/*Decide which side to go*/
            return node_search(comp, n->right, v);
         }else{
            return node_search(comp, n->left, v);
         }
      }else{/*Found v*/
         return true;
      }
   }else{/*reach the end*/
      return false;
   }
}

/*Look at the nodes searching for v return its pointer if found*/
bstnode* node_get(int(*comp)(const void* a, const void* b),
                bstnode* n, void* v){
   if(n){
      int dif = comp(n->data, v);
      if(dif != 0){
         if(dif < 0){/*Decide which side to go*/
            return node_get(comp, n->right, v);
         }else{
            return node_get(comp, n->left, v);
         }
      }else{/*Found v*/
         return n;
      }
   }else{/*reach the end*/
      return NULL;
   }
}

/*free a binary tree node*/
void node_free(bstnode** n){
   if(*n){
      bstnode* node = *n;

      node_free(&node->left);
      node_free(&node->right);
      free(node->data);
      free(*n);
      /* Helps to assert that the job has been done.*/
      *n = NULL;
   }
}

/*Print a node and all it`s childs*/
char* node_print(char*(*prnt)(const void* a), bstnode *n){
   if(n){
      char* leftStr = node_print(prnt, n->left);/*Get each side print*/
      char* rightStr = node_print(prnt, n->right);
      char* nodeStr = prnt(n->data);/*Get the node's print*/
      int size = strlen(leftStr)+strlen(rightStr)+strlen(nodeStr)+EXTRCHAR;
      /*calloc a new string capable of holding the three previous str*/
      char* str = (char*)calloc(size, sizeof(char));
      if(str){
         sprintf(str, "(%s%s%s)", nodeStr, leftStr, rightStr);
         free(leftStr);/*Free the previous calloced strs*/
         free(rightStr);
         return str;
      }else{
         ON_ERROR("Calloc faild on node_print. Insuficient memory.");
      }
   }else{/*base case - return an empty str if n is null*/
      char* str = (char*)calloc(EXTRCHAR, sizeof(char));
      if(str){
         return str;
      }else{
         ON_ERROR("Calloc faild on node_print. Insuficient memory.");
      }
   }
}

/*In-order Traversal*/
void node_traversal(bstnode *n, void **v, int size){
   if(n){
      /* *v is where I am in the array */
      node_traversal(n->left, v, size);
      *v = memcpy(*v, n->data, size);/*Copy the data to derref V*/
      *v = (void *)((char *)*v + size);/*Advance derref V to the next array cel*/
      node_traversal(n->right, v, size);
   }
}