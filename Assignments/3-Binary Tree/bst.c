#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "bst.h"

#define EXTRCHAR 3

/*Internal functions prototype*/
bstnode* node_init(int size, void* v);

bool node_insert(int(*comp)(const void* a, const void* b),
                 int size, bstnode** n, void* v);

int node_search(int(*comp)(const void* a, const void* b),
                bstnode* n, void* v);

int node_maxdepth(bstnode* n);

int node_count(bstnode* n);

void node_free(bstnode** n);

char* node_print(char*(*prnt)(const void* a), bstnode *n);

void node_traversal(bstnode *n, void **v, int size);

void bst_insertOrderedArr(bst* b, void* arr, int size, int start, int end);

/*Basic Functions*/
/*Initialize a binary search tree*/
bst* bst_init(int sz,
              int(*comp)(const void* a, const void* b),
              char*(*prnt)(const void* a)  ){
   if(sz && comp && prnt){
      bst* tree = (bst*)calloc(1, sizeof(bst));
      if(tree){
         tree->top = NULL;
         tree->elsz = sz;
         tree->compare = comp;
         tree->prntnode = prnt;
         return tree;
      }else{
         ON_ERROR("Calloc faild on bst_init. Insuficient memory.");
      }
   }else{
      ON_ERROR("Not enough parameters for bst_init.");
   }
}

/* Insert 1 item into the tree */
void bst_insert(bst* b, void* v){
   if(b){
      node_insert(b->compare, b->elsz, &b->top, v);
   }
}

/* Number of nodes in tree */
int bst_size(bst* b){
   if(b){
      return node_count(b->top);
   }else{
      return 0;
   }
}

/* Longest path from root to any leaf */
int bst_maxdepth(bst* b){
   if(b){
      return node_maxdepth(b->top);
   }else{
      return 0;
   }
}

/* Whether the data in v, is stored in the tree */
bool bst_isin(bst* b, void* v){
   if(b && v){
      return node_search(b->compare, b->top, v);
   }else{
      return false;
   }
}

/* Bulk insert n items from an array v into an initialised tree */
void bst_insertarray(bst* b, void* v, int n){
   if(b && v && n){
      int i = 0;
      while(i < n){
         bst_insert(b, v);
         v = (void*)((char *)v + b->elsz);
         i++;
      }
   }
}

/* Clear all memory associated with tree, & set pointer to NULL */
void bst_free(bst** p){
   if(p){
      bst* tree = *p;
      node_free(&tree->top);
      free(*p);
      *p = NULL;
   }
}

/*Advanced functionality*/
/* Return a string displaying the tree in a textual form (head(left)(right)) recursively */
/* USER MUST FREE the returning str*/
char* bst_print(bst* b){
   if(b){
      return node_print(b->prntnode, b->top);
   }else{
      return node_print(NULL, NULL);
   }
}

/* Fill an array with a copy of the sorted tree data */
void bst_getordered(bst* b, void* v){
   if(b && v){
      node_traversal(b->top, &v, b->elsz);
   }
}

/* Rebalance the tree, recursively using the median of the sorted keys */
bst* bst_rebalance(bst* b){
   int nNodes = bst_size(b);
   int size = b->elsz;
   void* arr = calloc(nNodes, size);
   bst* newB = bst_init(b->elsz, b->compare, b->prntnode);
   bst_getordered(b, arr);
   bst_insertOrderedArr(newB, arr, size, 0, nNodes-1);
   free(arr);
   return newB;
}

/*Insert elements from a ordered arr in a way that the resulting tree is balanced*/
void bst_insertOrderedArr(bst* b, void* arr, int size, int start, int end){
   if(start <= end){
      int middle = (start+end)/2;
      void *midPoint = (void*)((char*)arr+(middle*size));/*a pointer to arr's middle*/
      /*printf("%d - %d - %d - %d \n", start, end, middle, *(int*)midPoint);*/
      bst_insert(b, midPoint);/*Inser the arr's middle value*/
      bst_insertOrderedArr(b, arr, size, start, middle-1);/*Pass the left arr*/
      bst_insertOrderedArr(b, arr, size, middle+1, end);/*Pass the right arr*/
   }
}

/*internal functions*/
/*initialize a binary tree node*/
bstnode* node_init(int size, void* v){
   bstnode* n = (bstnode*)calloc(1, sizeof(bstnode));
   void* data = calloc(1, size);
   if(n && data){
      n->data = memcpy(data, v, size);
      n->left = NULL;
      n->right = NULL;
      return n;
   }else{
      ON_ERROR("Calloc faild on node_init. Insuficient memory.");
   }
}

/*insert a node in a binary tree*/
bool node_insert(int(*comp)(const void* a, const void* b),
                 int size, bstnode** n, void* v){
   if(*n){
      bstnode* nd = *n;
      int dif = comp(nd->data, v);
      if(dif != 0){
         if(dif < 0){/*Decide which side to go*/
            return node_insert(comp, size, &nd->right, v);
         }else{
            return node_insert(comp, size, &nd->left, v);
         }
      }else{/*We already have that value on our tree*/
         return false;
      }
   }else{/*If we reach a leaf*/
      *n = node_init(size, v);/*IF I use b->top instead of n it works*/
      return true;
   }
}

/*Return the number of nodes related to one note*/
int node_count(bstnode* n){
   int count = 0;
   if(n){
      count += 1;/*Count this node*/
      count += node_count(n->left);/*Count the nodes on the left*/
      count += node_count(n->right);/*Count the nodes on the right*/
   }
   return count;
}

/*Return the maxdepth related to one note*/
int node_maxdepth(bstnode* n){
   int count = 0;
   int leftDepth = 0;
   int rightDepth = 0;
   if(n){
      count += 1;/*Count this node*/
      leftDepth = node_maxdepth(n->left);/*Get the left max depth*/
      rightDepth = node_maxdepth(n->right);/*Get the right max depth*/
      count += (leftDepth > rightDepth) ? leftDepth : rightDepth;
   }
   return count;
}

/*Look at the nodes searching for v*/
int node_search(int(*comp)(const void* a, const void* b),
                bstnode* n, void* v){
   if(n){
      int dif = comp(n->data, v);
      if(dif != 0){
         if(dif < 0){/*Decide which side to go*/
            return node_search(comp, n->right, v);
         }else{
            return node_search(comp, n->left, v);
         }
      }else{/*Found v*/
         return true;
      }
   }else{/*reach the end*/
      return false;
   }
}

/*free a binary tree node*/
void node_free(bstnode** n){
   if(*n){
      bstnode* node = *n;

      node_free(&node->left);
      node_free(&node->right);
      free(node->data);
      free(*n);
      /* Helps to assert that the job has been done.*/
      *n = NULL;
   }
}

/*Print a node and all it`s childs*/
char* node_print(char*(*prnt)(const void* a), bstnode *n){
   if(n){
      char* leftStr = node_print(prnt, n->left);/*Get each side print*/
      char* rightStr = node_print(prnt, n->right);
      char* nodeStr = prnt(n->data);/*Get the node's print*/
      int size = strlen(leftStr)+strlen(rightStr)+strlen(nodeStr)+EXTRCHAR;
      /*calloc a new string capable of holding the three previous str*/
      char* str = (char*)calloc(size, sizeof(char));
      if(str){
         sprintf(str, "(%s%s%s)", nodeStr, leftStr, rightStr);
         free(leftStr);/*Free the previous calloced strs*/
         free(rightStr);
         return str;
      }else{
         ON_ERROR("Calloc faild on node_print. Insuficient memory.");
      }
   }else{/*base case - return an empty str if n is null*/
      char* str = (char*)calloc(EXTRCHAR, sizeof(char));
      if(str){
         return str;
      }else{
         ON_ERROR("Calloc faild on node_print. Insuficient memory.");
      }
   }
}

/*In-order Traversal*/
void node_traversal(bstnode *n, void **v, int size){
   if(n){
      /* *v is where I am in the array */
      node_traversal(n->left, v, size);
      *v = memcpy(*v, n->data, size);/*Copy the data to derref V*/
      *v = (void *)((char *)*v + size);/*Advance derref V to the next array cel*/
      node_traversal(n->right, v, size);
   }
}