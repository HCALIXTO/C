#include <stdio.h>
#include <stdlib.h>
#include "neillsdl2.h"

#define ON_ERROR(STR) fprintf(stderr, STR); exit(EXIT_FAILURE)
/*Structure defines*/
#define CELLSIZE 18
#define MILLISECONDDELAY 20
#define NLINES 25
#define NCOLS 40
/*Default Values Defines*/
#define DEFTG 0
#define DEFCOMMAND 0
/*Deouble Height Defines*/
#define TOPHALF 0
#define BOTTOMHALF 1
#define START 0
#define DOUBLESTEP 2
/*Sixel defines*/
#define SIXEL 6
#define SIXELH 3
#define SIXELW 2
#define OFF 0
#define ON 1
#define VONE 1
#define VTWO 2
#define VFOUR 4
#define VEIGHT 8
#define VSIXTEEN 16
#define VSIXTYFOUR 64
#define POSONE 0
#define POSTWO 1
#define POSFOUR 2
#define POSEIGHT 3
#define POSSIXTEEN 4
#define POSSIXTYFOUR 5
#define MINBLAST 32
#define MAXBLAST 64
/*Char/Command Values*/
#define MINCOM 128
#define MINCHAR 160
#define MAXCHAR 255
/*Colors*/
#define NUMCOLORS 8
#define NUMRGB 3
#define MINRGB 0
#define MAXRGB 255
/*Dimention base values*/
#define NUMHEIGHTS 2
#define NUMSOURCES 3
#define NUMMODES 2
#define NUMFUNCTIONS 32
#define FONT "m7fixed.fnt"
/*Command indexes*/
#define REDALPH 1
#define GREALPH 2
#define YELALPH 3
#define BLUALPH 4
#define MAGALPH 5
#define CYAALPH 6
#define WHIALPH 7
#define SHEIGHT 12
#define DHEIGHT 13
#define REDGRAPH 17
#define GREGRAPH 18
#define YELGRAPH 19
#define BLUGRAPH 20
#define MAGGRAPH 21
#define CYAGRAPH 22
#define WHIGRAPH 23
#define CONTGRAPH 25
#define SEPGRAPH 26
#define BLACKBACK 28
#define NEWBACK 29
#define HOLDGRAPH 30
#define RELEGRAPH 31
/*Dimentions are the cell's vars that sets how it looks*/
enum dimentions {fColor, bColor, height, source, mode, tg, command};
typedef enum dimentions dimentions;
/*used in the debug print, define how the dimention will translate to text*/
enum printModes {integer, hex, str};
typedef enum printModes printModes;
/*the available colors in the program*/
enum colors {red, green, yellow, blue, magenta, cyan, white, black};
typedef enum colors colors;
/*index to define red, green and blue*/
enum RGBindex {R, G, B};
typedef enum RGBindex RGBindex;
/*two heights modes*/
enum heights {doubleH, singleH};
typedef enum heights heights;
/*the three different types of characters*/
enum sources {text, contGraph, sepGraph};
typedef enum sources sources;
/*printing modes*/
enum modes {hold, release};
typedef enum modes modes;

/*Represents one "pixel" in our grid*/
struct cell {
   colors fColor;  /*Foreground color */
   colors bColor;  /*Background color */
   heights height; /*Cell's height */
   sources source; /*Text/Graph symbol source */
   modes mode;     /*Text/Graph mode(hold=last one - release=default)*/
   int tg;         /*Text/Graph value, int to look at the lookup table*/
   int command;    /*Value for this cell's hex command from the file*/
};
typedef struct cell cell;

/*Type of pointer to a command execution function*/
typedef void (*commandFunctions)(cell* cLine, cell* c);

/*The file structure*/
struct m7 {
   cell ***grid; /* Cell grid */
   cell *line;  /* A cell containing all the current values for the line*/
   cell *def;   /* A cell containing the default values for this struct*/
   commandFunctions *cFunc; /*pointer to array of command exec functions*/
   int numLines;/* Number of lines in the grid*/
   int numCols; /* Number of columns in the grid*/
};
typedef struct m7 m7;

/*Prototype*/
cell* cell_init(void);
void cell_free(cell** c);
void cell_default(cell *c);
cell* cell_get(m7* m, int line, int col);
m7* m7_init(int NLines, int NCols);
void m7_free(m7** m);
void m7_readFile(m7* m, const char *fileName);
void m7_printGrid(m7* m, dimentions d, printModes p);
void m7_print(m7* m);
void m7_addNewCellToGrid(m7* m, int line, int col, int command);
void m7_parseCell(m7* m, cell* c);

void copyAllButTGCommand(cell *source, cell *dest);
void cell_print(m7* m, cell *c, SDL_Simplewin *sw, int xCoor,
                     int yCoor, int line, int col);
void HNE_SDL_DrawChar(SDL_Simplewin *sw, 
                      fntrow fontdata[FNTCHARS][FNTHEIGHT],
                      int ox, int oy, cell* c);
void HNE_SDL_DrawDHeightChar(SDL_Simplewin *sw, 
                             fntrow fontdata[FNTCHARS][FNTHEIGHT],
                             int ox, int oy, cell* c, int half);
void HNE_SDL_DrawSixel(cell *c, SDL_Simplewin *sw, int xCoor,
                     int yCoor);
void BuildSixelStructure(int tg, int* structure);
int get_colorRGB(colors color, RGBindex index);
void runCommand(m7* m, cell* c, int command);
void buildCFunc(commandFunctions* lUp);
void redAlpha(cell* cLine, cell* c);
void greenAlpha(cell* cLine, cell* c);
void yellowAlpha(cell* cLine, cell* c);
void blueAlpha(cell* cLine, cell* c);
void magentaAlpha(cell* cLine, cell* c);
void cyanAlpha(cell* cLine, cell* c);
void whiteAlpha(cell* cLine, cell* c);
void singleHeight(cell* cLine, cell* c);
void doubleHeight(cell* cLine, cell* c);
void redGraph(cell* cLine, cell* c);
void greenGraph(cell* cLine, cell* c);
void yellowGraph(cell* cLine, cell* c);
void blueGraph(cell* cLine, cell* c);
void magentaGraph(cell* cLine, cell* c);
void cyanGraph(cell* cLine, cell* c);
void whiteGraph(cell* cLine, cell* c);
void contiguousGraph(cell* cLine, cell* c);
void separatedGraph(cell* cLine, cell* c);
void blackBack(cell* cLine, cell* c);
void newBack(cell* cLine, cell* c);
void holdGraph(cell* cLine, cell* c);
void releaseGraph(cell* cLine, cell* c);