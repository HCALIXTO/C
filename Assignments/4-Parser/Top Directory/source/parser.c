#include <stdio.h>
#include <stdlib.h>
#include "m7.h"

#define ON_ERROR(STR) fprintf(stderr, STR); exit(EXIT_FAILURE)
#define NLINES 25
#define NCODES 40

int main(int argc, char const *argv[]){
   m7* m;

   if(argc > 1){
      m = m7_init(NLINES, NCODES);
      m7_readFile(m,argv[1]);
      m7_print(m);
      m7_free(&m);
   }else{
      printf("Please run the program like: %s m7/test.m7 \n",argv[0]);
   }
   return 0;
}
