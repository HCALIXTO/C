#include "m7.h"

/*Cell Functions*/
/* Create a default cell */
cell* cell_init(void){
   cell* c;

   c = (cell*)calloc(1,sizeof(cell));
   if(c == NULL){
      ON_ERROR("Creation of Cell Failed\n");
   }
   cell_default(c);
   return c;
}

/* Clears all space used, and sets pointer to NULL */
void cell_free(cell** c){
   if(c){
      cell* cl = *c;
      free(cl);
      /* Helps to assert that the job has been done.*/
      *c = NULL;
   }
}

/* Set the cell dimentions to the default values*/ 
void cell_default(cell *c){
   c->fColor = white;
   c->bColor = black;
   c->height = singleH;
   c->source = text;
   c->mode = release;
   c->tg = DEFTG;
   c->command = DEFCOMMAND;
}

cell* cell_get(m7* m, int line, int col){
   if(m){
      if(line < m->numLines && col < m->numCols){
         return m->grid[line][col];
      }else{
         ON_ERROR("Line x Col coordinates out of m7 grid at \
          cell_get.\n");
      }
   }else{
      ON_ERROR("cell_get without m7.\n");
   }
}

/*Print the cell's characters, deals with height and source*/
void cell_print(m7* m, cell *c, SDL_Simplewin *sw, int xCoor,
                     int yCoor, int line, int col){
   if(c->source == text || (c->tg >= MINBLAST && c->tg < MAXBLAST)){
      fntrow fontdata[FNTCHARS][FNTHEIGHT];
      Neill_SDL_ReadFont(fontdata, FONT);
      if(c->height == singleH){
         HNE_SDL_DrawChar(sw, fontdata, xCoor, yCoor, c);
      }else{
         int half;
         /*Decide witch half of the character we will display*/
         if(line > 0){
            cell *topCel = cell_get(m, line-1, col);/*Get the cell above*/
            /*If the cell above has the same command and is double H, 
              we are at the bottom half - If I only check for top Cell double H
              I wouldnt be able to write two consecutive DH lines
              c->command == topCel->command && */
            if(topCel->height == doubleH){
               half = BOTTOMHALF;
            }else{
               half = TOPHALF;
            }
         }else{
            half = TOPHALF;
         }
         HNE_SDL_DrawDHeightChar(sw, fontdata, xCoor, yCoor, c, half);
      }
   }else{
      HNE_SDL_DrawSixel(c, sw, xCoor, yCoor);
   }
}

/*M7 functions*/
/* Create a m7 */
m7* m7_init(int NLines, int NCols){
   m7* m;
   int l;

   m = (m7*)calloc(1,sizeof(m7));
   if(m == NULL){
      ON_ERROR("Creation of m7 Failed\n");
   }
   m->numLines = NLines ? NLines : NLINES;
   m->numCols = NCols ? NCols : NCOLS;
   m->line = cell_init();
   m->def = cell_init();
   /*Create the grid that will receive all the cells/file instructions*/ 
   m->grid = (cell***)calloc(m->numLines,sizeof(cell**));
   for(l=0; l<m->numLines; l++){
      m->grid[l] = (cell**)calloc(m->numCols,sizeof(cell*));
   }
   /*Create and build command execution look up table*/
   m->cFunc = (commandFunctions*)calloc(NUMFUNCTIONS,sizeof(commandFunctions));
   buildCFunc(m->cFunc);

   return m;
}

/* Clears all space used, and sets pointer to NULL */
void m7_free(m7** m){
   if(m){
      int l, c;
      m7* mSeven = *m;
      for(l=0; l < mSeven->numLines; l++){
         for(c = 0; c < mSeven->numCols; c++){
            cell_free(&mSeven->grid[l][c]);
         }
         free(mSeven->grid[l]);
      }
      free(mSeven->grid);
      
      cell_free(&mSeven->line);
      cell_free(&mSeven->def);
      free(mSeven->cFunc);
      free(mSeven);
      /* Helps to assert that the job has been done.*/
      *m = NULL;
   }
}

/*Reads the inputed file*/
void m7_readFile(m7* m, const char *fileName){
   if(m){
      FILE *f;
      int com;
      int lineCount = 0;
      int colCount = 0;
      cell *c;

      f = fopen(fileName, "rt");
      if(f == NULL){
         ON_ERROR("Unable to open the given file.\n");
      }
      /*Ensure that I read the file until the end, or the max size*/
      while(((com = fgetc(f)) != EOF) && (lineCount < m->numLines)){
         com = com < MINCOM ? com+MINCOM : com;/*Compensate from the 7bit to 8*/
         /*Include a cell in the grid reflecting this command*/
         m7_addNewCellToGrid(m, lineCount, colCount, com);
         c = cell_get(m, lineCount, colCount);/*Get the new cell*/
         m7_parseCell(m, c);

         colCount++;
         if(colCount >= m->numCols){/*Go to next line*/
            /*Reset the current line config values*/
            copyAllButTGCommand(m->def,m->line);
            m->line->tg = DEFTG;
            m->line->command = DEFCOMMAND;
            /*Set the line and col counters*/
            colCount = 0;
            lineCount++;
         }
      }
      fclose(f);
   }
}

/*add a new default cell on m7's grid at the lineXcol with provided command value*/
void m7_addNewCellToGrid(m7* m, int line, int col, int command){
   if(m){
      if((line >= 0 && line < m->numLines) &&
         (col >= 0 && col < m->numCols)){
         cell* c = cell_init();
         c->command = command;
         m->grid[line][col] = c;
      }else{
         ON_ERROR("Line x Col coordinates out of m7 grid at \
          m7_addNewCellToGrid.\n");
      }
   }else{
      ON_ERROR("Not enough arguments at m7_addNewCellToGrid.\n");
   }

}

/*Interpret the cell's command and reflect it in the grid*/
void m7_parseCell(m7* m, cell* c){
   if(m){
      if(c->command >= MINCOM && c->command < MINCHAR){
         /*It is a config command*/
         int temp_com;
         temp_com = c->command-MINCOM;/*Bring the command to a range 0-32*/
         runCommand(m, c, temp_com);
      }else if(c->command >= MINCHAR && c->command <= MAXCHAR){
         /*It is a character to be printed*/
         int temp_char;
         temp_char = c->command-MINCHAR;/*Bring the char to a range 0-95*/
         m->line->tg = temp_char;/*Register this char as the last one*/
         copyAllButTGCommand(m->line, c);/*copy the current format to this cel*/
         c->tg = temp_char;/*Set this cell tg as this char*/
      }else{
         /*It is an error =)*/
         ON_ERROR("Command out of range.\n");
      }
   }else{
      ON_ERROR("Not enough arguments at m7_parseCell.\n");
   }
}

/*Print the m7 structure usind SDL*/
void m7_print(m7* m){
   if(m){
      int line, col;
      int y, x;
      cell *c;
      SDL_Simplewin sw;
      
      Neill_SDL_Init(&sw);
      /*Iterate through the grid*/
      for(line=0;line<m->numLines;line++){
         y = line*FNTHEIGHT;
         for(col=0;col<m->numCols;col++){
            x = col*FNTWIDTH;
            c = cell_get(m, line, col);/*Get the cell*/ 
            /*Print the cell*/
            cell_print(m, c, &sw, x, y, line, col);
         }
      }
      /*Let the window open until someone press something*/
      do{
         SDL_Delay(MILLISECONDDELAY);
         /* Has anyone pressed ESC or killed the SDL window ?
            Must be called frequently - it's the only way of escaping  */
         Neill_SDL_Events(&sw);
      }while(!sw.finished);
      /* Clear up graphics subsystems */
      atexit(SDL_Quit);
   }
}

/*Private functions*/

/*Cell functions*/
/*Copy all the dimentions but TG and Command, from source-dest*/
void copyAllButTGCommand(cell *source, cell *dest){
   dest->fColor = source->fColor;
   dest->bColor = source->bColor;
   dest->height = source->height;
   dest->source = source->source;
   dest->mode = source->mode;
}

/*Printing functions*/
/*Draw a character considering the cell's foreground and backgroung color*/
void HNE_SDL_DrawChar(SDL_Simplewin *sw, fntrow fontdata[FNTCHARS][FNTHEIGHT],
                      int ox, int oy, cell* c){
   unsigned x, y;
   for(y = 0; y < FNTHEIGHT; y++){
      for(x = 0; x < FNTWIDTH; x++){
         if(fontdata[c->tg][y] >> (FNTWIDTH - 1 - x) & 1){
            Neill_SDL_SetDrawColour(sw, get_colorRGB(c->fColor, R),
                                     get_colorRGB(c->fColor, G),
                                     get_colorRGB(c->fColor, B));
            SDL_RenderDrawPoint(sw->renderer, x + ox, y+oy);
         }
         else{
            Neill_SDL_SetDrawColour(sw, get_colorRGB(c->bColor, R),
                                     get_colorRGB(c->bColor, G),
                                     get_colorRGB(c->bColor, B));
            SDL_RenderDrawPoint(sw->renderer, x + ox, y+oy);
         }
      }
   }
   Neill_SDL_UpdateScreen(sw);
}

/*Draw a double height character*/
void HNE_SDL_DrawDHeightChar(SDL_Simplewin *sw, 
                             fntrow fontdata[FNTCHARS][FNTHEIGHT],
                             int ox, int oy, cell* c, int half){
   unsigned x, y;
   int lineCount = START;
   unsigned lStart, lStop;
   /*Chose where to start and finish based on witch half we are*/
   if(half == TOPHALF){
      lStart = START;
      lStop = FNTHEIGHT/NUMHEIGHTS;
   }else{
      lStart = FNTHEIGHT/NUMHEIGHTS;
      lStop = FNTHEIGHT;
   }
   for(y = lStart; y < lStop; y++){
      for(x = 0; x < FNTWIDTH; x++){
         if(fontdata[c->tg][y] >> (FNTWIDTH - 1 - x) & 1){
            Neill_SDL_SetDrawColour(sw, get_colorRGB(c->fColor, R),
                                     get_colorRGB(c->fColor, G),
                                     get_colorRGB(c->fColor, B));
            /*Draw in two pixels, one above the other*/
            SDL_RenderDrawPoint(sw->renderer, x + ox, lineCount+oy);
            SDL_RenderDrawPoint(sw->renderer, x + ox, lineCount+oy+1);
         }
         else{
            Neill_SDL_SetDrawColour(sw, get_colorRGB(c->bColor, R),
                                     get_colorRGB(c->bColor, G),
                                     get_colorRGB(c->bColor, B));
            /*Draw in two pixels, one above the other*/
            SDL_RenderDrawPoint(sw->renderer, x + ox, lineCount+oy);
            SDL_RenderDrawPoint(sw->renderer, x + ox, lineCount+oy+1);
         }
      }
      /*Progress the line counter by two, since we draw in two lines*/
      lineCount+=DOUBLESTEP;
   }
   Neill_SDL_UpdateScreen(sw);
}

/*Draw a cell sixel char*/ 
void HNE_SDL_DrawSixel(cell *c, SDL_Simplewin *sw, int xCoor,
                     int yCoor){
   int structure[SIXEL] = {OFF};
   int sixHOffSet, sixWOffSet, i;
   SDL_Rect sixel;

   sixel.w = FNTWIDTH/SIXELW;
   sixel.h = FNTHEIGHT/SIXELH;

   sixHOffSet = 0;
   sixWOffSet = 0;
   
   BuildSixelStructure(c->tg, structure);
   for (i = 0; i < SIXEL; ++i){
      sixel.x = xCoor+sixWOffSet;
      sixel.y = yCoor+sixHOffSet;
      if(structure[i]){
         /*If the square is full, I draw the square in the fColor*/
         Neill_SDL_SetDrawColour(sw, get_colorRGB(c->fColor, R),
                              get_colorRGB(c->fColor, G),
                              get_colorRGB(c->fColor, B));
         /* Filled Rectangle*/
         SDL_RenderFillRect(sw->renderer, &sixel);

         Neill_SDL_UpdateScreen(sw);
         if(c->source == sepGraph){/*Draw the separations in the background color*/
            Neill_SDL_SetDrawColour(sw, get_colorRGB(c->bColor, R),
                              get_colorRGB(c->bColor, G),
                              get_colorRGB(c->bColor, B));
            /* Line Rectangle*/
            SDL_RenderDrawRect(sw->renderer, &sixel);

            Neill_SDL_UpdateScreen(sw);
         }
      }else{
         Neill_SDL_SetDrawColour(sw, get_colorRGB(c->bColor, R),
                              get_colorRGB(c->bColor, G),
                              get_colorRGB(c->bColor, B));
         /* Fill Rectangle*/
         SDL_RenderFillRect(sw->renderer, &sixel);

         Neill_SDL_UpdateScreen(sw);
      }
      if(i%2 == 0){
         sixWOffSet = sixWOffSet+sixel.w;
      }else{
         sixWOffSet = sixWOffSet-sixel.w;
         sixHOffSet = sixHOffSet+sixel.h;
      }
   }
}

/*Based on the tg value, tells witch of the six slots should be on*/
void BuildSixelStructure(int tg, int* structure){
   if(tg>=VSIXTYFOUR){
      structure[POSSIXTYFOUR] = ON;
      tg = tg - VSIXTYFOUR;
   }
   if(tg>=VSIXTEEN){
      structure[POSSIXTEEN] = ON;
      tg = tg - VSIXTEEN;
   }
   if(tg>=VEIGHT){
      structure[POSEIGHT] = ON;
      tg = tg - VEIGHT;
   }
   if(tg>=VFOUR){
      structure[POSFOUR] = ON;
      tg = tg - VFOUR;
   }
   if(tg>=VTWO){
      structure[POSTWO] = ON;
      tg = tg - VTWO;
   }
   if(tg>=VONE){
      structure[POSONE] = ON;
      tg = tg - VONE;
   }
}

/*Return the color coordinates in an array*/
int get_colorRGB(colors color, RGBindex index){
   int *colorIndex[NUMCOLORS];
   int r[NUMRGB] = {MAXRGB, MINRGB, MINRGB};
   int g[NUMRGB] = {MINRGB, MAXRGB, MINRGB};
   int y[NUMRGB] = {MAXRGB, MAXRGB, MINRGB};
   int b[NUMRGB] = {MINRGB, MINRGB, MAXRGB};
   int m[NUMRGB] = {MAXRGB, MINRGB, MAXRGB};
   int c[NUMRGB] = {MINRGB, MAXRGB, MAXRGB};
   int w[NUMRGB] = {MAXRGB, MAXRGB, MAXRGB};
   int bk[NUMRGB] = {MINRGB, MINRGB, MINRGB};
   colorIndex[red] = r;
   colorIndex[green] = g;
   colorIndex[yellow] = y;
   colorIndex[blue] = b;
   colorIndex[magenta] = m;
   colorIndex[cyan] = c;
   colorIndex[white] = w;
   colorIndex[black] = bk;

   return colorIndex[color][index];
}

/*Command functions*/
/*Run the cell's command*/
void runCommand(m7* m, cell* c, int command){
   if(m->cFunc[command]){
      /*Check if we are at hold mode, and put the last val if we are*/
      if(m->line->mode == hold){
         c->tg = m->line->tg;
      }else{
         c->tg = DEFTG;
      }
      /*Run the cells command*/
      m->cFunc[command](m->line, c);
   }
}

/*Build the commant execution functions look up table*/
void buildCFunc(commandFunctions* lUp){
   lUp[REDALPH] = redAlpha;
   lUp[GREALPH] = greenAlpha;
   lUp[YELALPH] = yellowAlpha;
   lUp[BLUALPH] = blueAlpha;
   lUp[MAGALPH] = magentaAlpha;
   lUp[CYAALPH] = cyanAlpha;
   lUp[WHIALPH] = whiteAlpha;
   lUp[SHEIGHT] = singleHeight;
   lUp[DHEIGHT] = doubleHeight;
   lUp[REDGRAPH] = redGraph;
   lUp[GREGRAPH] = greenGraph;
   lUp[YELGRAPH] = yellowGraph;
   lUp[BLUGRAPH] = blueGraph;
   lUp[MAGGRAPH] = magentaGraph;
   lUp[CYAGRAPH] = cyanGraph;
   lUp[WHIGRAPH] = whiteGraph;
   lUp[CONTGRAPH] = contiguousGraph;
   lUp[SEPGRAPH] = separatedGraph;
   lUp[BLACKBACK] = blackBack;
   lUp[NEWBACK] = newBack;
   lUp[HOLDGRAPH] = holdGraph;
   lUp[RELEGRAPH] = releaseGraph;
}

/*Red Alphanumeric command*/
void redAlpha(cell* cLine, cell* c){
   cLine->fColor = red;
   cLine->source = text;
   copyAllButTGCommand(cLine, c);
}

/*Green Alphanumeric command*/
void greenAlpha(cell* cLine, cell* c){
   cLine->fColor = green;
   cLine->source = text;
   copyAllButTGCommand(cLine, c);
}

/*Yellow Alphanumeric command*/
void yellowAlpha(cell* cLine, cell* c){
   cLine->fColor = yellow;
   cLine->source = text;
   copyAllButTGCommand(cLine, c);
}

/*Blue Alphanumeric command*/
void blueAlpha(cell* cLine, cell* c){
   cLine->fColor = blue;
   cLine->source = text;
   copyAllButTGCommand(cLine, c);
}

/*Magenta Alphanumeric command*/
void magentaAlpha(cell* cLine, cell* c){
   cLine->fColor = magenta;
   cLine->source = text;
   copyAllButTGCommand(cLine, c);
}

/*Cyan Alphanumeric command*/
void cyanAlpha(cell* cLine, cell* c){
   cLine->fColor = cyan;
   cLine->source = text;
   copyAllButTGCommand(cLine, c);
}

/*White Alphanumeric command*/
void whiteAlpha(cell* cLine, cell* c){
   cLine->fColor = white;
   cLine->source = text;
   copyAllButTGCommand(cLine, c);
}

/*Single Height command*/
void singleHeight(cell* cLine, cell* c){
   cLine->height = singleH;
   copyAllButTGCommand(cLine, c);
   releaseGraph(cLine,c);
}

/*Double Height command*/
void doubleHeight(cell* cLine, cell* c){
   cLine->height = doubleH;
   copyAllButTGCommand(cLine, c);
   releaseGraph(cLine,c);
}

/*Red Graphics command*/
void redGraph(cell* cLine, cell* c){
   cLine->fColor = red;
   cLine->source = contGraph;
   copyAllButTGCommand(cLine, c);
}

/*Green Graphics command*/
void greenGraph(cell* cLine, cell* c){
   cLine->fColor = green;
   cLine->source = contGraph;
   copyAllButTGCommand(cLine, c);
}

/*Yellow Graphics command*/
void yellowGraph(cell* cLine, cell* c){
   cLine->fColor = yellow;
   cLine->source = contGraph;
   copyAllButTGCommand(cLine, c);
}

/*Blue Graphics command*/
void blueGraph(cell* cLine, cell* c){
   cLine->fColor = blue;
   cLine->source = contGraph;
   copyAllButTGCommand(cLine, c);
}

/*Magenta Graphics command*/
void magentaGraph(cell* cLine, cell* c){
   cLine->fColor = magenta;
   cLine->source = contGraph;
   copyAllButTGCommand(cLine, c);
}

/*Cyan Graphics command*/
void cyanGraph(cell* cLine, cell* c){
   cLine->fColor = cyan;
   cLine->source = contGraph;
   copyAllButTGCommand(cLine, c);
}

/*White Graphics command */
void whiteGraph(cell* cLine, cell* c){
   cLine->fColor = white;
   cLine->source = contGraph;
   copyAllButTGCommand(cLine, c);
}

/*Contiguous Graphics command - source become the contiguous graph*/
void contiguousGraph(cell* cLine, cell* c){
   cLine->source = contGraph;
   copyAllButTGCommand(cLine, c);
   releaseGraph(cLine,c);
}

/*Separated Graphics command - source become the separated graph*/
void separatedGraph(cell* cLine, cell* c){
   cLine->source = sepGraph;
   copyAllButTGCommand(cLine, c);
   releaseGraph(cLine,c);
}

/*Black Background command*/
void blackBack(cell* cLine, cell* c){
   cLine->bColor = black;
   copyAllButTGCommand(cLine, c);
}

/*New Background command - backgound color become the foreground*/
void newBack(cell* cLine, cell* c){
   cLine->bColor = cLine->fColor;
   copyAllButTGCommand(cLine, c);
}

/*Hold Graphics command - the display value is the last one*/
void holdGraph(cell* cLine, cell* c){
   cLine->mode = hold;
   copyAllButTGCommand(cLine, c);
   c->tg = cLine->tg;
}

/*Release Graphics command - the display value is the last one*/
void releaseGraph(cell* cLine, cell* c){
   cLine->mode = release;
   copyAllButTGCommand(cLine, c);
   c->tg = DEFTG;
}

/*Debuging functions*/
/*Print one dimention at a time, great way to debug the grid*/
void m7_printGrid(m7* m, dimentions d, printModes p){
   if(m){
      const char *format;
      char cl_name[NUMCOLORS] = {'r','g','y','b','m','c','w','b'};
      char hg_name[NUMHEIGHTS] = {'D','S'};
      char sr_name[NUMSOURCES] = {'T','C','S'};
      char md_name[NUMMODES] = {'H','R'};
      int l, c;

      switch(p){
         case(str):
         format = "%s ";
         break;

         case(hex):
         format = "%x ";
         break;

         default:
         case(integer):
         format = "%d ";
         break;
      }

      switch(d){
         case(fColor):
         for(l=0;l<m->numLines;l++){
            for(c=0;c<m->numCols;c++){
               if(p == str){
                  printf("%c ", cl_name[m->grid[l][c]->fColor]);
               }else{
                  printf(format, m->grid[l][c]->fColor);
               }
            }
            printf("- Line: %d\n", l+1);
         }
         break;

         case(bColor):
         for(l=0;l<m->numLines;l++){
            for(c=0;c<m->numCols;c++){
               if(p == str){
                  printf("%c ", cl_name[m->grid[l][c]->bColor]);
               }else{
                  printf(format, m->grid[l][c]->bColor);
               }
            }
            printf("- Line: %d\n", l+1);
         }
         break;

         case(height):
         for(l=0;l<m->numLines;l++){
            for(c=0;c<m->numCols;c++){
               if(p == str){
                  printf("%c ", hg_name[m->grid[l][c]->height]);
               }else{
                  printf(format, m->grid[l][c]->height);
               }
            }
            printf("- Line: %d\n", l+1);
         }
         break;

         case(source):
         for(l=0;l<m->numLines;l++){
            for(c=0;c<m->numCols;c++){
               if(p == str){
                  printf("%c ", sr_name[m->grid[l][c]->source]);
               }else{
                  printf(format, m->grid[l][c]->source);
               }
            }
            printf("- Line: %d\n", l+1);
         }
         break;

         case(mode):
         for(l=0;l<m->numLines;l++){
            for(c=0;c<m->numCols;c++){
               if(p == str){
                  printf("%c ", md_name[m->grid[l][c]->mode]);
               }else{
                  printf(format, m->grid[l][c]->mode);
               }
            }
            printf("- Line: %d\n", l+1);
         }
         break;

         case(tg):
         for(l=0;l<m->numLines;l++){
            for(c=0;c<m->numCols;c++){
               printf(format, m->grid[l][c]->tg);
            }
            printf("- Line: %d\n", l+1);
         }
         break;

         default:
         case(command):
         for(l=0;l<m->numLines;l++){
            for(c=0;c<m->numCols;c++){
               printf(format, m->grid[l][c]->command);
            }
            printf("- Line: %d\n", l+1);
         }
         break;
      }

   }
}