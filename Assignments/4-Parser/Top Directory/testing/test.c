#include <stdio.h>
#include <stdlib.h>
#include "../source/m7.h"
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define LINET 3
#define COLT 5
#define NDIMENTIONS 7
#define NTESTSPERLINE 6
#define NTESTLINES 7
/*File defines*/
#define TESTFILE "testFiles/test.m7"
/*Dimentions indexes*/
#define FCI 0
#define BCI 1
#define HEI 2
#define SOI 3
#define MDI 4
#define TGI 5
#define CDI 6
/*Sixels Index*/
#define SIXEL1 160
#define SIXEL2 175
#define SIXEL3 176
#define SIXEL4 185
#define SIXEL5 237
#define SIXEL6 247
#define SIXEL7 255
#define NSIXEL 6
/*Char indexes*/
#define TESTCHAR1 MINCHAR+5
#define TESTCHAR2 MAXCHAR-5
/*Command indexes*/
#define COMREDALPH REDALPH + MINCOM
#define COMGREALPH GREALPH + MINCOM
#define COMYELALPH YELALPH + MINCOM
#define COMBLUALPH BLUALPH + MINCOM
#define COMMAGALPH MAGALPH + MINCOM
#define COMCYAALPH CYAALPH + MINCOM
#define COMWHIALPH WHIALPH + MINCOM
#define COMSHEIGHT SHEIGHT + MINCOM
#define COMDHEIGHT DHEIGHT + MINCOM
#define COMREDGRAPH REDGRAPH + MINCOM
#define COMGREGRAPH GREGRAPH + MINCOM
#define COMYELGRAPH YELGRAPH + MINCOM
#define COMBLUGRAPH BLUGRAPH + MINCOM
#define COMMAGGRAPH MAGGRAPH + MINCOM
#define COMCYAGRAPH CYAGRAPH + MINCOM
#define COMWHIGRAPH WHIGRAPH + MINCOM
#define COMCONTGRAPH CONTGRAPH + MINCOM
#define COMSEPGRAPH SEPGRAPH + MINCOM
#define COMBLACKBACK BLACKBACK+ MINCOM
#define COMNEWBACK NEWBACK + MINCOM
#define COMHOLDGRAPH HOLDGRAPH+ MINCOM
#define COMRELEGRAPH RELEGRAPH+ MINCOM

/*Prototype*/
void support_test(void);
void test_BuildSixelStructure(void);
void test_get_colorRGB(void);
void cell_test(void);
void cell_test_init_free(void);
void cell_test_default(void);
void cell_test_copy(void);
void m7_test(void);
void m7_test_init_free(void);
void m7_test_grid(void);
void m7_test_command(void);
void m7_test_readFile(void);

void cell_into_array(cell* c, int* a);
int compare_arrays(int* a1, int*a2,int len);
void print_arrays(int* a1, int len);

int main(void){
   support_test();
   cell_test();
   m7_test();
   return 0;
}

/*SUPPORT FUNCTIONS*/
/*Test all the support functions (don't depends of other functions)*/
void support_test(void){
   printf("Initializing support functions tests: \n");
   test_BuildSixelStructure();
   test_get_colorRGB();
   printf("Support functions tests successfully done. \n");
}

/*Test the coordinates for the sixels*/
void test_BuildSixelStructure(void){
   int cmds[NTESTLINES] = {\
      SIXEL1,\
      SIXEL2,\
      SIXEL3,\
      SIXEL4,\
      SIXEL5,\
      SIXEL6,\
      SIXEL7\
   };
   int resp[NTESTLINES][NSIXEL] = {\
      {OFF, OFF, OFF, OFF, OFF, OFF},\
      {ON, ON, ON, ON, OFF, OFF},\
      {OFF, OFF, OFF, OFF, ON, OFF},\
      {ON, OFF, OFF, ON, ON, OFF},\
      {ON, OFF, ON, ON, OFF, ON},\
      {ON, ON, ON, OFF, ON, ON},\
      {ON, ON, ON, ON, ON, ON}\
   };
   int line;

   printf("Testing: Build Sixel Structure - ");
   
   for(line = 0; line < NTESTLINES; line++){
      int coordinates[NSIXEL] = {OFF};
      /*I subtract the min char to emulate what happen on parsing*/
      BuildSixelStructure(cmds[line]-MINCHAR, coordinates);
      assert(compare_arrays(coordinates, resp[line], NSIXEL)==TRUE);
   }
   printf("OK.\n");
}

/*Test the coordinates for the sixels*/
void test_get_colorRGB(void){
   int cmds[NTESTLINES] = {\
      red,\
      green,\
      yellow,\
      blue,\
      magenta,\
      cyan,\
      white\
   };
   int resp[NTESTLINES][NUMRGB] = {\
      {MAXRGB, MINRGB, MINRGB},\
      {MINRGB, MAXRGB, MINRGB},\
      {MAXRGB, MAXRGB, MINRGB},\
      {MINRGB, MINRGB, MAXRGB},\
      {MAXRGB, MINRGB, MAXRGB},\
      {MINRGB, MAXRGB, MAXRGB},\
      {MAXRGB, MAXRGB, MAXRGB}\
   };
   int line;

   printf("Testing: Get RGB Colors - ");
   
   for(line = 0; line < NTESTLINES; line++){
      assert((get_colorRGB(cmds[line], R)-resp[line][R])==0);
      assert((get_colorRGB(cmds[line], G)-resp[line][G])==0);
      assert((get_colorRGB(cmds[line], B)-resp[line][B])==0);
   }
   printf("OK.\n");
}

/*CELL*/
/*Test all the cell specific functions*/
void cell_test(void){
   printf("Initializing cell tests: \n");
   cell_test_init_free();
   cell_test_default();
   cell_test_copy();

   printf("Cell tests successfully done. \n");
}

/*Test cell initialization and free*/
void cell_test_init_free(void){
   cell* c = cell_init();
   printf("Testing: cell init and free - ");
   
   assert(c != NULL);
   /*Assert that the initial cell values are the default ones*/
   assert(c->fColor == white);
   assert(c->bColor == black);
   assert(c->height == singleH);
   assert(c->source == text);
   assert(c->mode == release);
   assert(c->tg == DEFTG);
   assert(c->command == DEFCOMMAND);
   cell_free(&c);
   assert(c == NULL);
   printf("OK.\n");
}

/*Test the function that set the cell's dimentions to it's default values*/
void cell_test_default(void){
   cell* c = cell_init();
   printf("Testing: cell default - ");
   
   c->fColor = black;
   c->bColor = white;
   c->height = doubleH;
   c->source = sepGraph;
   c->mode = hold;
   c->tg = DHEIGHT;
   c->command = DHEIGHT;
   cell_default(c);
   assert(c->fColor == white);
   assert(c->bColor == black);
   assert(c->height == singleH);
   assert(c->source == text);
   assert(c->mode == release);
   assert(c->tg == DEFTG);
   assert(c->command == DEFCOMMAND);
   cell_free(&c);
   printf("OK.\n");
}

/*Test the function that set the cell's dimentions to it's default values*/
void cell_test_copy(void){
   cell* source = cell_init();
   cell* dest = cell_init();
   printf("Testing: cell copy - ");
   
   source->fColor = black;
   source->bColor = white;
   source->height = doubleH;
   source->source = sepGraph;
   source->mode = hold;
   source->tg = DHEIGHT;
   source->command = DHEIGHT;
   /*Assert that the dest is with the default values*/
   assert(dest->fColor == white);
   assert(dest->bColor == black);
   assert(dest->height == singleH);
   assert(dest->source == text);
   assert(dest->mode == release);
   assert(dest->tg == DEFTG);
   assert(dest->command == DEFCOMMAND);
   copyAllButTGCommand(source, dest);
   /*Assert that the dest values match with the source, except tg and command*/
   assert(dest->fColor == source->fColor);
   assert(dest->bColor == source->bColor);
   assert(dest->height == source->height);
   assert(dest->source == source->source);
   assert(dest->mode == source->mode);
   assert(dest->tg != source->tg);
   assert(dest->command != source->command);
   cell_free(&dest);
   cell_free(&source);
   printf("OK.\n");
}

/*M7*/
/*Test all the m7 specific functions*/
void m7_test(void){
   printf("Initializing m7 tests: \n");
   m7_test_init_free();
   m7_test_grid();
   m7_test_command();
   m7_test_readFile();
   printf("M7 tests successfully done. \n");
}

/*Test m7 initialization and free*/
void m7_test_init_free(void){
   m7* m = m7_init(NLINES, NCOLS);
   printf("Testing: m7 init and free - ");
   
   assert(m != NULL);
   /*Assert that the initial m7 values are the default ones*/
   assert(m->numLines == NLINES);
   assert(m->numCols == NCOLS);
   assert(m->line != NULL);
   assert(m->def != NULL);
   assert(m->grid != NULL);
   assert(m->cFunc != NULL);
   m7_free(&m);
   assert(m == NULL);
   printf("OK.\n");
}

/*Test insert, get and parse a Cell in a m7 grid*/
void m7_test_grid(void){
   m7* m = m7_init(NLINES, NCOLS);
   cell* c;
   printf("Testing: m7 grid - ");
   m7_addNewCellToGrid(m, LINET, COLT, REDGRAPH+MINCOM);
   c = cell_get(m, LINET, COLT);/*Get the new cell*/
   assert(c != NULL);
   /*Check if command is the provided one and the other dimentions are default*/
   assert(c->command == REDGRAPH+MINCOM);
   assert(c->fColor == white);
   assert(c->bColor == black);
   assert(c->height == singleH);
   assert(c->source == text);
   assert(c->mode == release);
   assert(c->tg == DEFTG);
   m7_parseCell(m, c);
   /*The command is to activate the red graph, so to check if it was 
     parsed I can check if fcolor is red and source is contiguous graph*/ 
   assert(c->fColor == red);
   assert(c->source == contGraph);
   /*It also should reflect the change on m7's line (to keep 
     track of the accumulated changes)*/
   assert(m->line->fColor == red);
   assert(m->line->source == contGraph);
   m7_free(&m);
   printf("OK.\n");
}

/*Test the commands execution and it's cumulative effects
  this tests: m7 and cell init, cell insertion, get, parse and copy
  command execution and each command functions effects on cell*/
void m7_test_command(void){
   int line = 0, col = 0;
   int cellArray[NDIMENTIONS] = {0};
   m7* m = m7_init(NLINES, NCOLS);
   cell* c;
   int cmds[NTESTLINES][NTESTSPERLINE] = {\
      {COMREDALPH,COMNEWBACK,COMDHEIGHT,COMHOLDGRAPH,TESTCHAR1,COMGREALPH},\
      {TESTCHAR2,COMHOLDGRAPH,COMWHIALPH,COMNEWBACK,COMBLUGRAPH,COMSEPGRAPH},\
      {TESTCHAR2,COMHOLDGRAPH,COMMAGGRAPH,COMNEWBACK,COMRELEGRAPH,COMBLACKBACK},\
      {TESTCHAR2,COMHOLDGRAPH,COMYELALPH,COMNEWBACK,COMSEPGRAPH,COMBLACKBACK},\
      {COMDHEIGHT,TESTCHAR1,COMHOLDGRAPH,COMREDGRAPH,COMRELEGRAPH,COMSHEIGHT},\
      {TESTCHAR2,COMHOLDGRAPH,COMGREALPH,COMSEPGRAPH,COMNEWBACK,COMCYAGRAPH},\
      {TESTCHAR2,COMHOLDGRAPH,COMCYAALPH,COMNEWBACK,COMBLUGRAPH,COMCYAGRAPH}\
   };
   int resp[NTESTLINES][NDIMENTIONS] = {\
      {green, red, doubleH, text, hold, TESTCHAR1-MINCHAR, COMGREALPH},\
      {blue, white, singleH, sepGraph, release, DEFTG, COMSEPGRAPH},\
      {magenta, black, singleH, contGraph, release, DEFTG, COMBLACKBACK},\
      {yellow, black, singleH, sepGraph, release, DEFTG, COMBLACKBACK},\
      {red, black, singleH, contGraph, release, DEFTG, COMSHEIGHT},\
      {cyan, green, singleH, contGraph, release, DEFTG, COMCYAGRAPH},\
      {cyan, cyan, singleH, contGraph, hold, TESTCHAR2-MINCHAR, COMCYAGRAPH}\
   };
   printf("Testing: m7 commands - ");
   
   for(line = 0; line < NTESTLINES; line++){
      for(col = 0; col < NTESTSPERLINE; col++){
         m7_addNewCellToGrid(m, line, col, cmds[line][col]);
         c = cell_get(m, line, col);
         m7_parseCell(m, c);
      }
      cell_into_array(c, cellArray);
      assert(compare_arrays(cellArray, resp[line], NDIMENTIONS)==TRUE);
      cell_default(m->line);/*Set the line keeper to default*/
   }
   m7_free(&m);
   printf("OK.\n");
}

/*The test file is a small file with the same commands we defined in cmds at
  m7_test_command, so in this function I read this file and check the end of
  each of it's lines to see if it maches with what it should be
  This is a final test, and encapsules the whole program structure, to ensure 
  that all the parts are behaving together as they should*/ 
void m7_test_readFile(void){
   m7* m;
   cell* c;
   int line;
   int cellArray[NDIMENTIONS] = {0};
   int resp[NTESTLINES][NDIMENTIONS] = {\
      {green, red, doubleH, text, hold, TESTCHAR1-MINCHAR, COMGREALPH},\
      {blue, white, singleH, sepGraph, release, DEFTG, COMSEPGRAPH},\
      {magenta, black, singleH, contGraph, release, DEFTG, COMBLACKBACK},\
      {yellow, black, singleH, sepGraph, release, DEFTG, COMBLACKBACK},\
      {red, black, singleH, contGraph, release, DEFTG, COMSHEIGHT},\
      {cyan, green, singleH, contGraph, release, DEFTG, COMCYAGRAPH},\
      {cyan, cyan, singleH, contGraph, hold, TESTCHAR2-MINCHAR, COMCYAGRAPH}\
   };
   printf("Testing: m7 read file - ");
   m = m7_init(NTESTLINES, NTESTSPERLINE);
   m7_readFile(m,TESTFILE);
   /*m7_printGrid(m, command, hex);
   m7_print(m);*/
   for(line = 0; line < NTESTLINES; line++){
      c = cell_get(m, line, NTESTSPERLINE-1);
      cell_into_array(c, cellArray);
      assert(compare_arrays(cellArray, resp[line], NDIMENTIONS)==TRUE);
   }
   m7_free(&m);
   printf("OK.\n");
}
/*Support functions*/

/*put a cell's dimentions into an array. Simplify array dimentions comparison*/
void cell_into_array(cell* c, int* a){
   a[FCI] = (int)c->fColor;
   a[BCI] = (int)c->bColor;
   a[HEI] = (int)c->height;
   a[SOI] = (int)c->source;
   a[MDI] = (int)c->mode;
   a[TGI] = c->tg;
   a[CDI] = c->command;
}

/*compare the values of two arrays*/
int compare_arrays(int* a1, int*a2,int len){
   int i;
   /*print_arrays(a1, len);
   print_arrays(a2, len);*/
   for(i = 0; i<len; i++){
      if(a1[i] != a2[i]){
         return FALSE;
      }
   }
   return TRUE;
}
/*print the values of an array*/
void print_arrays(int* a1, int len){
   int i;
   printf("[");
   for(i = 0; i<len; i++){
      printf("%d ", a1[i]);
   }
   printf("]\n");
}
