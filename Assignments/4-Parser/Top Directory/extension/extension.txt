Hi, for my extension I implemented a script that 
reads an image and reproduces it as an m7 file. 

In order to be able to work with a more complex 
colour structure, I’ve chosen to reproduce the 
image as a grid of 25x40 pixels using m7 available
colours. This approach have a really small number
of pixels, therefore It can only process simple 
images.

To read and manipulate the image file I’ve 
installed ImageMagick 7, the most recent version,
and used magickWand (it’s C API), the necessary
includes to compile are in the makefile.

In general, my script reduces the image to 25x40
pixels (add padding if necessary), do a simple 
threshold  to translate the pixel colour to the m7
corresponding one and finally translate this colours
distribution to a set of hex instructions on a m7 file.

Despite the output limitations, this extension was 
interesting for me. Since it gave me the opportunity 
to work with an image library, understand a little of 
how images works, how to manipulate and how to gather
information from them.

You can run the program by calling ./ext img/t1.jpg m7/t1.m7
you should try the flags they are really nice =)

Best regards Henrique Calixto de Oliveira - ho17459