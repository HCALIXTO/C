#include <stdio.h>
#include <stdlib.h>
/*#include <MagickCore/MagickCore.h>*/
#include <MagickWand/MagickWand.h>
#include "../source/m7.h"

#define HLF 2
#define DOUBLE 2
#define NARGS 3
#define EXECIND 0
#define IMGIND 1
#define FILEIND 2
#define IMGW NCOLS
#define IMGH NLINES
#define REDCOM REDALPH + MINCOM
#define GRECOM GREALPH + MINCOM
#define YELCOM YELALPH + MINCOM
#define BLUCOM BLUALPH + MINCOM
#define MAGCOM MAGALPH + MINCOM
#define CYACOM CYAALPH + MINCOM
#define WHICOM WHIALPH + MINCOM
#define NEWBCOM NEWBACK + MINCOM
#define BBACKCOM BLACKBACK + MINCOM
#define ThrowWandException(wand) \
{ \
   char \
      *description; \
   \
   ExceptionType \
      severity; \
   \
   description=MagickGetException(wand,&severity); \
   (void) fprintf(stderr,"%s %s %lu %s\n",GetMagickModule(),description); \
   description=(char *) MagickRelinquishMemory(description); \
   exit(-1); \
}

/*Prototype*/
void img_to_m7(MagickWand *magick_wand, FILE *fp);
void prepare_img(MagickWand *magick_wand, size_t hTgt, size_t wTgt);
void generate_commands_from_color_map(int cmd[IMGH][IMGW],\
     colors c_map[IMGH][IMGW]);
void print_commands(int cmd[IMGH][IMGW]);
void print_commands_to_file(int cmd[IMGH][IMGW], FILE *fp);
void extract_color_from_img(MagickWand *magick_wand,\
     colors c_map[IMGH][IMGW]);
colors color_from_pixel(PixelInfo pixel);

int main(int argc,char **argv){
   FILE *fp;
   MagickBooleanType status;
   MagickWand *magick_wand;
   m7* m;

   if (argc != NARGS){
      (void) fprintf(stdout,"Usage: %s img/t1.jpg m7/t1.m7\n",argv[EXECIND]);
      exit(0);
   }
   /*Read an image.*/
   MagickWandGenesis();
   magick_wand=NewMagickWand();
   status=MagickReadImage(magick_wand,argv[IMGIND]);
   if (status == MagickFalse){
      ThrowWandException(magick_wand);
   }
   /*Translate image to m7 file*/
   fp = fopen(argv[FILEIND],"wb+");
   img_to_m7(magick_wand, fp);
   fclose(fp);

   /*Display the image in m7 format*/
   m = m7_init(IMGH, IMGW);
   m7_readFile(m,argv[FILEIND]);
   m7_print(m);
   m7_free(&m);
   
   magick_wand=DestroyMagickWand(magick_wand);
   MagickWandTerminus();
   return(0);
}

/*Wraper that gether all the functions that transforms an img to a .m7 file*/
void img_to_m7(MagickWand *magick_wand, FILE *fp){
   colors c_map[IMGH][IMGW];
   int commands[IMGH][IMGW];

   prepare_img(magick_wand, IMGH, IMGW);
   extract_color_from_img(magick_wand, c_map);
   generate_commands_from_color_map(commands, c_map);
   /*print_commands(commands);*/
   print_commands_to_file(commands, fp);
}

/*Resize the image to the m7 dimentions, where each pixel will represent one cell*/
void prepare_img(MagickWand *magick_wand, size_t hTgt, size_t wTgt){
   size_t width, height;
   double hXw;

   height = MagickGetImageHeight(magick_wand);
   width = MagickGetImageWidth(magick_wand);
   hXw = (double)height/width;
   /* Resize the image to the m7 size*/
   MagickResetIterator(magick_wand);
   while (MagickNextImage(magick_wand) != MagickFalse){
      /*Reduce the colours*/
      MagickQuantizeImage(magick_wand, NUMCOLORS, RGBColorspace,\
                          0, UndefinedDitherMethod, 0);
      /*Scale the images*/
      if(hXw < 0){
         MagickScaleImage(magick_wand,wTgt,wTgt*hXw);
         
      }else{
         MagickScaleImage(magick_wand,hTgt/hXw,hTgt);

      }
      /*Add the white padding to make it in the m7 proportion*/
      height = MagickGetImageHeight(magick_wand);
      width = MagickGetImageWidth(magick_wand);
      MagickExtentImage(magick_wand,wTgt,hTgt,-(wTgt-width)/HLF,\
                        -(hTgt-height)/HLF);  
   }
}

/*Translate the color map to a list of commands*/
void generate_commands_from_color_map(int cmd[IMGH][IMGW],\
     colors c_map[IMGH][IMGW]){
   int y, x;
   colors cNext;
   int cTable[NUMCOLORS] = {REDCOM, GRECOM, YELCOM, BLUCOM, \
              MAGCOM, CYACOM, WHICOM, BBACKCOM};
   for(y = 0; y < IMGH; y++){
      /*I go two by two due to m7 way of setting the background color*/
      for (x = 0; x < IMGW; x+=DOUBLE){
         cNext = c_map[y][x+1];/*Get the next pixel's color*/
         cmd[y][x] = cTable[cNext];
         cmd[y][x+1] = NEWBCOM;
         if(cNext == black){
            cmd[y][x] = cTable[c_map[y][x]];
            cmd[y][x+1] = cTable[cNext];
         }
      }
      
   }
}

/*Printe the commands on the console - For debbuging*/
void print_commands(int cmd[IMGH][IMGW]){
   int y, x;
   printf("\n");
   for(y = 0; y < IMGH; y++){
      for (x = 0; x < IMGW; x++){
         printf("%x ", cmd[y][x]);
      }
      printf("\n");
   }
}

/*Write the commands to file*/
void print_commands_to_file(int cmd[IMGH][IMGW], FILE *fp){
   int y, x;
   short i;
   for(y = 0; y < IMGH; y++){
      for (x = 0; x < IMGW; x++){
         i = cmd[y][x];
         fwrite( &i, sizeof(i)/HLF, 1, fp );
      }
   }
}

/*Read the image pixel and extract a color structure from it*/
void extract_color_from_img(MagickWand *magick_wand,\
     colors c_map[IMGH][IMGW]){
   PixelIterator * iter;
   PixelWand ** row;
   PixelInfo pixel;
   size_t width, height, y, x;
   /*iterate all its pixels*/
   height = MagickGetImageHeight(magick_wand);
   width = MagickGetImageWidth(magick_wand);
   iter = NewPixelIterator(magick_wand);
   for(y=0; y<height ; y++){
      /*Pull all pixels in a row.*/
      row = PixelGetNextIteratorRow(iter, &width);
      /*Iterate over all pixels collected.*/
      for (x = 0; x < width; ++x){
         /*Copy pixel data to packet.*/
         PixelGetMagickColor(row[x], &pixel);
         /*Get Pixel colors.*/
         c_map[y][x] = color_from_pixel(pixel);
      }
   }
}

/*Return a color approximation based on the pixel color*/
colors color_from_pixel(PixelInfo pixel){
   int r, g, b;
   r = ((pixel.red/QuantumRange*MAXRGB)>MAXRGB/HLF)? 1 : 0;
   g = ((pixel.green/QuantumRange*MAXRGB)>MAXRGB/HLF)? 1 : 0;
   b = ((pixel.blue/QuantumRange*MAXRGB)>MAXRGB/HLF)? 1 : 0;
   if(r && g && b){
      return white;
   }else if(r && g){
      return yellow;
   }else if(r && b){
      return magenta;
   }else if(g && b){
      return cyan;
   }else if(r){
      return red;
   }else if(g){
      return green;
   }else if(b){
      return blue;
   }else{
      return black;
   }
}

/* http://www.multipole.org/discourse-server/viewtopic.php?t=18116
http://www.imagemagick.org/api/magick-image.php?ImageMagick=s75kt9vbdb2h4klf40ouav2j10#MagickQuantizeImage
https://stackoverflow.com/questions/5642621/what-is-the-magickwand-equivalent-of-the-colors-option

http://www.fmwconcepts.com/imagemagick/pixelize/index.php
https://www.imagemagick.org/api/MagickWand/magick-image_8c_source.html
http://www.imagemagick.org/MagickWand/extent.htm
http://www.multipole.org/discourse-server/viewtopic.php?t=19323
*/