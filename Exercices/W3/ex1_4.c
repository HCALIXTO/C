#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define MAXLEN 100

int checkPalindrome(int len,char *s);

void getText(char *s, int *len);

int main(void){
   int len = 0;
   char s[MAXLEN];
   printf("\nWrite the text you want to check for palindrome\n");
   
   getText(s, &len);
   /*printf("%s\n",s);*/
   if(checkPalindrome(len, s)){
      printf("It is palindrome\n");
   }else{
      printf("It is NOT palindrome\n");
   }
   return 0;
   
}

void getText(char *s, int *len){
   int c;
   *len = 0;
   while((c = getchar()) != EOF && c != '\n' && *len < MAXLEN){
      if(c >= 'A' && c <= 'Z'){
         c=tolower(c);/*make it lower case*/
      }
      /*Now I check if is lower to ignore sapaces, points etc*/
      if(islower(c)){
         *s++ = c; /*make the val of where s points equal c and make s points to the next memory*/
         *len = *len+1;
      }
   }
   *s = '\0'; 
}

int checkPalindrome(int len,char *s){
   int start = 0, end = len-1;
   if(len > start){
   /*Increase the start and decrease the end, until I reach the middle of the array*/
      for(start = 0; start <= end; start++, end--){
         if(s[start] != s[end]){
            return 0;
         }
      }
      return 1;
   }else{
      return 0;
   }
}
