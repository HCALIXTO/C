#include <stdio.h>
#include <stdlib.h>

void int2string(int i,char *s);

int main(void){
   int i;
   char s[256];
   printf("Write a number and see what string it is\n");
   while(scanf("%d",&i) == 1 && i != EOF){
      int2string(i,s);
      printf("%s\n",s);
   }

   return 0;
   
}

void int2string(int i,char *s){
   *s++ = i;/*set first char to i then add one to pointer*/
   *s = '\0';/*must end string with null char*/
}
