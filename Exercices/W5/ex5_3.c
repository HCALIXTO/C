#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LINELEN 4
#define NLINES 2900
#define FILENAME "34words.txt"

void printArray(int arr[], int len);

void printWords(char arr[], int len);

void permute(int *arr, int len, int pos);

void swap(int arr[], int p1, int p2);

int main(int argc, char const *argv[]) {
  FILE *f;
  char word[10];
  char words[NLINES][LINELEN];
  int arr[] = {1,2,3,4};
  char c;
  int iWord = 0,iList = 0;

  f = fopen(FILENAME, "r");
  if(f == NULL){
    printf("Unable to open file.\n");
    exit(1);
  }
  while((c = getc(f)) != EOF){
    if(c == '\n'){
      /*words[iList][iWord] = '\0';*/
      iWord = 0;
      iList++;
    }else{
      /*words[iList][iWord] = c;*/
    }
    iWord++;
  }
  printWords(words, NLINES);
  if(argc == 3){
    if(strlen(argv[1]) == 4 && strlen(argv[2]) == 4){
      printf("%s\n", argv[1] );
      printf("%s\n", argv[2] );
      permute(arr, 3, 0);
    }else{
      printf("ERRO: Incorrect usage, please input only four letter words.\n");
    }
  }else{
     printf("ERRO: Incorrect usage, try e.g. %s TIME LIVE\n",argv[0]);
  }

  return 0;
}

void permute(int *arr, int len, int pos){
  if(len == pos){
    printArray(arr, len);
    return;
  }else{
    int j;
    for(j = pos; j <= len; j++){
      swap(arr, pos, j);
      permute(arr, len, pos+1);
      swap(arr, pos, j);
    }
  }
}

void swap(int arr[], int p1, int p2){
  int temp;
  temp = arr[p1];
  arr[p1] = arr[p2];
  arr[p2] = temp;
}


void printArray(int arr[], int len){
  int i;
  for(i=0; i<=len; i++){
    printf("%d", arr[i]);
  }
  printf("\n");
}

void printWords(char arr[], int len){
  int i;
  for(i=0; i<=len; i++){
    printf("%s\n", arr[i]);
  }
  printf("\n");
}
