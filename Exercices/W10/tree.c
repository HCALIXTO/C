#include <stdio.h>
#include <stdlib.h>

struct treeNode{
   struct treeNode *parent;
   struct treeNode **childs;
   int numChilds;
   Board *info;
};

typedef struct treeNode TreeNode;