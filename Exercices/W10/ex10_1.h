#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

#define BWIDTH 7
#define BHEIGHT 8
#define FREE 0
#define OCCUPIED 1
#define FALSE 0
#define TRUE 1
#define FAIL 0
#define SUCCESS 1

struct board{
   int status;
   int *grid[BHEIGHT][BWIDTH];
};

typedef struct board Board;

Board* initialBoard(){
   Board* b = malloc(sizeof(Board));
   b->status = FAIL;
   b->grid = malloc(sizeof(int g[BHEIGHT][BWIDTH]));
}