#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXSTRING 11

struct node{
   Board *info;
   struct node *next;
};

typedef struct node Node;

Node *AllocateNode(Board *b);

void addNextNode(Node *prev, Node *new);

void insertionSort(Node *start, Board *b);

void PrintList(Node *n);

Node *InList(Node *n, Board *b);
