#include <stdio.h>
#include <stdlib.h>

void randomlyPrint(int n);

int delimitedRandom(int min, int max);

int main(void){
   int nSongs = 0;
   
   printf("\nHow many songs?\n");
   while(scanf("%d", &nSongs) == 1 && nSongs != EOF){
      randomlyPrint(nSongs);
      printf("\nHow many songs?\n");
   }
   return 0;
}

void randomlyPrint(int n){
   int index = 0, nOut = 0;
   int *arr;
   arr = (int *)calloc(n, sizeof(int));
   if(arr){
      printf("\n");
      while (nOut != n){
         /*Get a random number from the beggining to the    end of the array*/
         index = delimitedRandom(0, n-1);
         if(!arr[index]){/*Check if this number have been printed previously*/
            printf("%d ",index+1);/*I dont want to print 0*/
            arr[index] = 1;
            nOut++;
         }
      }
      printf("\n");
   }else{
      printf("Sorry mate, we could not generate an array.");
   }
}

int delimitedRandom(int min, int max){
   return (rand()%(max-min+1))+min;
}
