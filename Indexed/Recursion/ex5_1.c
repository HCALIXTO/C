#include <stdio.h>
#include <stdlib.h>
#define STARTX 0
#define STARTY 1
#define ROWS 3
#define COLS 3
#define MAXMOVES ROWS * COLS
#define OCCUPIED 1
#define FREE 0

enum sides {bottom, left, top, right};

typedef enum sides sides;

struct position{
   int x;
   int y;
};

typedef struct position position;

int explore(position pos, int maze[ROWS][COLS], int nMoves);

int isValidPosition(position pos, int maze[ROWS][COLS]);

void nextPosition(position *pos, sides s);

void setGrid(int grid[ROWS][COLS]);

void printGrid(int grid[ROWS][COLS]);

int main(void) {
   int nPaths = 0;
   int grid[ROWS][COLS];
   position pos;
   pos.x = STARTX;
   pos.y = STARTY;
   /*Set the grid to all free*/
   setGrid(grid);
   /*Discover the number of possible paths*/
   nPaths = explore(pos, grid, 1);
   printf("The number of possible paths is: %d\n", nPaths);
   return 0;
}

int explore(position pos, int maze[ROWS][COLS], int nMoves){
   int counter = 0;
   sides s;
   position next;
   if(nMoves >= MAXMOVES){/*Base Case*/
     /*If we reach the maximun number of movements
     I return 1 to indicate that we found one path*/
     counter++;
   }else{/*Keep digging*/
     maze[pos.y][pos.x] = OCCUPIED;/*Register the move*/
     printGrid(maze);/* -FOR DEBUGGING-*/
     /*Iterate through all possible sides*/
     for(s = bottom; s <= right; s++){
        /*Reset the next position to be equal the actual position*/
        next.x = pos.x;
        next.y = pos.y;
        nextPosition(&next,s);/*set the next to be the current side*/
        if(isValidPosition(next, maze)){/*Check if is a valid next movement*/
          /*been a valid position I keep moving until I reach the basecase
          or run out of possible movements*/
          counter = counter + explore(next, maze, nMoves+1);
          /*here I also add the number of possible paths to the counter in
          orther to return all of them*/
        }
      }
      maze[pos.y][pos.x] = FREE;/*Free the space for next try*/
   }
   return counter;
}

int isValidPosition(position pos, int maze[ROWS][COLS]){
   /*Check if I am in the array scope and
   if my position is not occupied*/
   if(pos.x < 0 || pos.x >= COLS){
      return 0;
   }
   if(pos.y < 0 || pos.y >= ROWS){
      return 0;
   }
   if(maze[pos.y][pos.x] == OCCUPIED){
      return 0;
   }
   return 1;
}

void nextPosition(position *pos, sides s){
  /*Set the position based on the desired side*/
  switch (s) {
    case bottom:
       pos->x = pos->x-1;
       break;
    case left:
       pos->y = pos->y-1;
       break;
    case top:
       pos->x = pos->x+1;
       break;
    case right:
    default:
       pos->y = pos->y+1;
       break;
  }
}

void setGrid(int grid[ROWS][COLS]){
   /*Set all the cells in the grid to free*/
   int r, c;
   for(r = 0; r<ROWS ; r++){
      for(c=0; c<COLS ; c++){
         grid[r][c] = FREE;
      }
   }
}

void printGrid(int grid[ROWS][COLS]){
   /*Print the grid as text on the terminal*/
   int r, c;
   for(r = 0; r < ROWS; r++){
      for(c = 0; c < COLS; c++){
         printf("%d", grid[r][c]);
      }
      printf("\n");
   }
   printf("\n");
}
