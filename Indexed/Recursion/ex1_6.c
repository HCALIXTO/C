#include <stdio.h>
#define MAX 50000
#define NOPRINT 0
#define YESPRINT 1

int hailstonator(int n, int print, int *length, int *largest);

int main(void){
   int maxLength = 0, largest = 0, maxLengthN = 0, largestN = 0;
   int prevLargest, length;
   int n;
   
   for(n = 1; n <= MAX; n++){
      prevLargest = largest;
      length = 0;
      
      hailstonator(n, NOPRINT, &length, &largest);
      /*Update the max length and it's generator, if we have a new one*/
      if(length > maxLength){
         maxLength = length;
         maxLengthN = n;
      }
      /*Updates the largest number generator if we have a new largest*/
      if(largest != prevLargest){
         largestN = n;
      }
   }

	printf("The number that generates the longest hailstone is %d,\n", maxLengthN);
	printf("the sequence is %d numbers long.\n", maxLength);
	printf("The number that leads to the largest number is %d\n", largestN);
	printf("The largest number in all the sequences is %d\n", largest);
	
	/*Uncomment to see how they look
	hailstonator(maxLengthN, YESPRINT, &length, &largest);
	printf("\n\n\n");
	hailstonator(largestN, YESPRINT, &length, &largest);
   */
   
   return 0;
}

int hailstonator(int n, int print, int *length, int *largest){
   /*Add one to THIS hailstone length*/
   *length = *length+1;
   /*Check if we have a new largest number ever*/
   if(n > *largest){
      *largest = n;
   }
   /*Base case. "<=" to catch if you try to hailstone 0*/
   if(n <= 1){
      if(print){
         printf("%d \n", n);
      }
      /*Stop the recursion*/
      return 1; 
   }else{
      if(print){
         printf("%d, ", n);
      }
      /*Go to the next recursion*/
      if(n%2 == 0){
         return hailstonator(n/2, print, length, largest);
      }else{
         return hailstonator((3*n)+1, print, length, largest);
      }
   }
}

