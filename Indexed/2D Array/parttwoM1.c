#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 21
#define MIDDLE (N/2)
#define NREP (N*N*N*N)
#define START 0
#define FINISH N-1
#define MIN 0
#define MAX 9

void setBoard(int board[N][N]);

void printBoard(int board[N][N]);

void mutateBoard(int board[N][N]);

int delimitedRandom(int min, int max);

int otherRandom(int min, int max, int num);

int main(void)
{
	int board[N][N];
	int count = 0;

	srand(time(NULL));/*seed the random with time, must do once in main*/

	setBoard(board);
	printBoard(board);

	while(count < NREP){
		mutateBoard(board);
		count++;
	}
	printf("\n\n");

	printBoard(board);

	return 0;
}

void setBoard(int board[N][N]){
	int row, col;

	for(row = 0; row < N; row++){
		for(col=0; col < N; col++){
			board[row][col] = delimitedRandom(MIN, MAX);
		}
	}
}

void printBoard(int board[N][N]){
	int row, col;

	for(row = 0; row < N; row++){
		for(col=0; col < N; col++){
			printf("%d",board[row][col]);
		}
		printf("\n");
	}
}

void mutateBoard(int board[N][N]){
	int row1 = delimitedRandom(START, FINISH);
	int row2 = otherRandom(START, FINISH, row1);
	int col1 = delimitedRandom(START, FINISH);
	int col2 = otherRandom(START, FINISH, col1);
	int dist1 = 0;
	int dist2 = 0;
	int drow1 = 0;
	int drow2 = 0;
	int dcol1 = 0;
	int dcol2 = 0;
	int temp;

	if(row1 > MIDDLE){
		drow1 = row1 - MIDDLE;
	}else{
		drow1 = -(row1 - MIDDLE);
	}
	if(col1 > MIDDLE){
		dcol1 = col1 - MIDDLE;
	}else{
		dcol1 = -(col1 - MIDDLE);
	}
	if(row2 > MIDDLE){
		drow2 = row2 - MIDDLE;
	}else{
		drow2 = -(row2 - MIDDLE);
	}
	if(col2 > MIDDLE){
		dcol2 = col2 - MIDDLE;
	}else{
		dcol2 = -(col2 - MIDDLE);
	}
	dist1 = drow1+dcol1;
	dist2 = drow2+dcol2;
	/*First step*/
	if(dist1 < dist2){
		if(board[row1][col1] > board[row2][col2]){
			temp = board[row1][col1];
			board[row1][col1] = board[row2][col2];
			board[row2][col2] = temp;
		}
	}else{
		if(board[row2][col2] > board[row1][col1]){
			temp = board[row1][col1];
			board[row1][col1] = board[row2][col2];
			board[row2][col2] = temp;
		}
	}
}

int delimitedRandom(int min, int max){
   return (rand()%(max-min+1))+min;
}

int otherRandom(int min, int max, int num){
	int n;
	do{
		n = delimitedRandom(min, max);
	}while(n == num);
	return n;
}