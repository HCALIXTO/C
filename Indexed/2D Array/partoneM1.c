#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 20
#define NREP (N*N*N)
#define START 0
#define FINISH N-1
#define MIN 0
#define MAX 9

void setBoard(int board[N][N]);

void printBoard(int board[N][N]);

void mutateBoard(int board[N][N]);

int delimitedRandom(int min, int max);

int otherRandom(int min, int max, int num);

int main(void)
{
	int board[N][N];
	int count = 0;

	srand(time(NULL));/*seed the random with time, must do once in main*/

	setBoard(board);
	printBoard(board);

	while(count < NREP){
		mutateBoard(board);
		count++;
	}
	printf("\n\n");
	printBoard(board);

	return 0;
}

void setBoard(int board[N][N]){
	int row, col;

	for(row = 0; row < N; row++){
		for(col=0; col < N; col++){
			board[row][col] = delimitedRandom(MIN, MAX);
		}
	}
}

void printBoard(int board[N][N]){
	int row, col;

	for(row = 0; row < N; row++){
		for(col=0; col < N; col++){
			printf("%d",board[row][col]);
		}
		printf("\n");
	}
}

void mutateBoard(int board[N][N]){
	int row1 = delimitedRandom(START, FINISH);
	int row2 = otherRandom(START, FINISH, row1);
	int col1 = delimitedRandom(START, FINISH);
	int col2 = otherRandom(START, FINISH, col1);
	int colL = (col1 > col2)? col2: col1;
	int colR = (col1 < col2)? col2: col1;
	int rowT = (row1 > row2)? row2: row1;
	int rowB = (row1 < row2)? row2: row1;
	int temp;
	/*First step*/
	if(board[row1][colL] > board[row1][colR]){
		temp = board[row1][colL];
		board[row1][colL] = board[row1][colR];
		board[row1][colR] = temp;
	}
	if(board[rowT][col1] > board[rowB][col1]){
		temp = board[rowT][col1];
		board[rowT][col1] = board[rowB][col1];
		board[rowB][col1] = temp;
	}
}

int delimitedRandom(int min, int max){
   return (rand()%(max-min+1))+min;
}

int otherRandom(int min, int max, int num){
	int n;
	do{
		n = delimitedRandom(min, max);
	}while(n == num);
	return n;
}