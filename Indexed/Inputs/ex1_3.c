#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int scode(int a);

int main(void){

   int c, secretC;

	while((c = getchar()) != EOF){
		
		secretC = scode(c);
		putchar(secretC);
		
	}

   return 0;

}

int scode(int a){
   /*Transform a to z, B to Y and so on*/
   int temp = a;
   if( isupper(a) ){
      temp = ('Z' - a) + 'A';
   }
   if( islower(a) ){
      temp = ('z' - a) + 'a';
   }
   
   return temp;
}
