#include "LList.h"

Node *AllocateNode(char *s){
   Node *p;

   p = (Node *)malloc(sizeof(Node));

   if(p == NULL){
      printf("We had a problem with malloc, that shouldn`t have happened.\n");
      exit(1);
   }

   strcpy(p->word, s);
   p->next = NULL;

   return p;
}

void addNextNode(Node *prev, Node *new){
  new->next = prev->next;
  prev->next = new;
}

void insertionSort(Node *start, char *word){
   int needToInsert = 1;
   Node *current = start, *new = AllocateNode(word);
   while(current->next && needToInsert){
     /*I check the next node and insert the new between this and the next*/
      if(strcmp(current->next->word, new->word) > 0){
        addNextNode(current, new);
        needToInsert = 0;
      }
      current = current->next;/*Move to next link*/
   }
   /*If I reach the end and didn't insert the word*/
   if(needToInsert){
     current->next = new;/*New is the new end*/
   }
}

void PrintList(Node *n){
   /*BASE CASE*/
   if(n == NULL){
      return;
   }

   printf("Word: %s\n", n->word);
   PrintList(n->next);
}

Node *InList(Node *n, char *word){
   /*BASE CASE*/
   if(n == NULL){
      return n;
   }
   /*check if the actual word is the searched one*/
   if(strcmp(n->word, word) == 0){
      return n;/*if it is return it*/
   }
   /*If not, go to look at the next word*/
   return InList(n->next, word);
}
