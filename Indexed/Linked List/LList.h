#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXSTRING 11

struct node{
   char word[MAXSTRING];
   struct node *next;
};

typedef struct node Node;

Node *AllocateNode(char *s);

void addNextNode(Node *prev, Node *new);

void insertionSort(Node *start, char *word);

void PrintList(Node *n);

Node *InList(Node *n, char *word);
