#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "set.h"

#define INITSETSIZE 0
#define FIRSTPOS 0
#define MINSIZE 0
#define EQUAL 0
#define FALSE 0
#define TRUE 1
#define NOTFOUND -1

/*POINTS TO CHECK:
-IS OK to ruturn the error that I returned in the case of a
set_removeone from an empty set?
- My approach of defining multiple names fro 0 is ok?

*/

/*Internal functions prototype*/
int find_element(set* s, arrtype l);

void pop_from_position(set* s, int p);

arrtype get_from_position(set* s, int p);

/* Create empty set */
set* set_init(void){
   set* s;

   s = calloc(1,sizeof(set));
   if(s == NULL){
      ON_ERROR("Creation of Set Failed\n");
   }
   s->ua = arr_init();
   s->sz = INITSETSIZE;
   return s;
}

/* Create new set, copied from another */
set* set_copy(set* s){
   set* newS;

   newS = set_init();
   while(set_size(newS) < set_size(s)){
      set_insert(newS, get_from_position(s, set_size(newS)));
   }
   return newS;
}

/* Create new set, copied from an array of length n*/
set* set_fromarray(arrtype* a, int n){
   set* newS;
   int pos;

   newS = set_init();
   for(pos=FIRSTPOS ;pos < n; pos++){
      set_insert(newS, *a);
      a++;
   }
   return newS;
}

/* Basic Operations */
/* Add one element into the set */
void set_insert(set* s, arrtype l){
   if(!set_contains(s, l) && s){
      arr_set(s->ua, s->sz, l);
      s->sz += 1;
   }
}

/* Return size of the set */
int set_size(set* s){
   if(s){
      return s->sz;
   }else{
      return INITSETSIZE;
   }
}

/* Returns true if l is in the array, false elsewise */
int set_contains(set* s, arrtype l){
   if(find_element( s, l) != NOTFOUND){
      return TRUE;
   }else{
      return FALSE;
   }
}

/* Remove l from the set (if it's in) */
void set_remove(set* s, arrtype l){
   pop_from_position(s, find_element(s, l));
}

/* Remove one element from the set - there's no
   particular order for the elements, so any will do */
arrtype set_removeone(set* s){
   arrtype el;

   if(set_size(s) > 0){
      el = get_from_position(s, FIRSTPOS);
      pop_from_position(s, FIRSTPOS);
      return el;
   }else{
      ON_ERROR("Can't remove from an empty set.\n");
   }
}

/* Operations on 2 sets */
/* Create a new set, containing all elements from s1 & s2 */
set* set_union(set* s1, set* s2){
   set* newS;
   int pos;

   newS = set_init();
   for(pos = FIRSTPOS;pos < set_size(s1); pos++){
      set_insert(newS, get_from_position(s1, pos));
   }
   for(pos = FIRSTPOS;pos < set_size(s2); pos++){
      set_insert(newS, get_from_position(s2, pos));
   }
   return newS;
}

/* Create a new set, containing all elements in both s1 & s2 */
set* set_intersection(set* s1, set* s2){
   set* newS;
   arrtype el;
   int pos;

   newS = set_init();
   for(pos = FIRSTPOS;pos < set_size(s1); pos++){
      el = get_from_position(s1, pos);
      if(set_contains(s2, el)){
         set_insert(newS, el);
      }
   }
   return newS;
}

/* Finish up */
/* Clears all space used, and sets pointer to NULL */
void set_free(set** s){
   if(s){
      set* st = *s;
      arr** a = &st->ua;
      arr_free(a);
      free(st);
      /* Helps to assert that the job has been done.*/
      *s = NULL;
   }
}

/*Internal functions*/
/*Return the index of an element or -1 if it is not in the set*/
int find_element(set* s, arrtype l){
   if(s){
      int stop = set_size(s)-1, i;
      arrtype el;

      for(i=FIRSTPOS; i <= stop; i++){
         el = get_from_position(s, i);
         /*memcmp to compare memory independent of type*/
         if(memcmp(&el, &l, sizeof(arrtype)) == EQUAL){
            return i;
         }
      }
      return NOTFOUND;
   }else{
      return NOTFOUND;
   }
}

/*Pop a element from the desired position*/
void pop_from_position(set* s, int p){
   if(s){
      int stop = set_size(s)-1;
      /*Check if I`m in the set scope and if the set has anything*/
      if(p >= FIRSTPOS && p <= stop){
         for(; p < stop; p++){
            /*Set the arr[position] to arr[position+1]*/ 
            arr_set(s->ua, p, get_from_position(s, p+1));
         }
         s->sz -= 1;/*Reduce the size*/
      }
   }
}

/*Get a element from the desired position*/
arrtype get_from_position(set* s, int p){
   if(s){
      int stop = set_size(s)-1;
      /*Check if I`m in the set scope and if the set has anything*/
      if(p >= FIRSTPOS && p <= stop){
         return arr_get(s->ua, p);
      }else{
         ON_ERROR("Requested position is not allowed.\n");
      }
   }else{
      ON_ERROR("Requested position from a NULL set.\n");
   }
}
