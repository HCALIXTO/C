#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "set.h"

#define PRIDE "pride-and-prej.txt"
#define SENSE "sense-and-sense.txt"
#define NULLCHAR '\0'

/*
"pride-and-prej.txt"
"sense-and-sense.txt"
"t1.txt"
"t2.txt"
*/

void printSet(set *s);

void put_file_into_set(set *s, char *fileName);

int main(void){
   set *sSet, *pSet, *cSet;

   sSet = set_init();
   put_file_into_set(sSet, SENSE);
   printf("There are %d unique words in %s\n", set_size(sSet), SENSE);

   pSet = set_init();
   put_file_into_set(pSet, PRIDE);
   printf("There are %d unique words in %s\n", set_size(pSet), PRIDE);

   cSet = set_intersection(sSet, pSet);
   printf("There are %d common words\n", set_size(cSet));
   /*printSet(pSet);*/

   return 0;
}

/*Reads a file's lines and put into a set*/
void put_file_into_set(set *s, char *fileName){
   FILE *f;
   arrtype el;

   f = fopen(fileName, "r");
   if(f == NULL){
      printf("Unable to open %s.\n", fileName);
      exit(1);
   }
   memset(el.str, NULLCHAR, sizeof(el.str));
   while(fscanf(f, "%s", el.str) > 0){
      /*printf("%s\n", el.str);*/
      set_insert(s, el);
      memset(el.str, NULLCHAR, sizeof(el.str));
   }
}

/*Print a string set*/
void printSet(set *s){
   arrtype el;
   printf("Set size: %d\n", set_size(s));
   while(set_size(s) > 0){
      el = set_removeone(s);
      printf(" - %s\n", el.str);
   }
}