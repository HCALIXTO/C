#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 15
#define MIN 0
#define MAX 199
#define NREP 1000

int delimitedRandom(int min, int max);

void printArray(int arr[N]);

void setArrayRand(int arr[N]);

void checkRep(int arr[N]);

int inArr(int arr[N], int num);

int main(void){
	int arr[N] = {0};
	int count;

	srand(time(NULL));/*seed the random with time, must do once in main*/

	for(count = 0; count < NREP; count++){
		setArrayRand(arr);
		printArray(arr);
		checkRep(arr);
		printf("\n");
	}

	return 0;
}

void setArrayRand(int arr[N]){
	int i = 0;

	while(i < N){
		arr[i] = delimitedRandom(MIN, MAX);
		i++;
	}
}

void checkRep(int arr[N]){
	int col, num, i, count;
	int check[N] = {MAX+1};

	for(col=0; col < N; col++){
		num = arr[col];
		if(!inArr(check, num)){
			check[col] = num; /*So I dont count it twice*/
			count = 0;

			for(i = col; i < N; i++){
				if(num == arr[i]){
					count++;
				}
			}

			if(count > 1){
				printf("( %d appears %d times )", num, count);
			}
		}
	}
}

int inArr(int arr[N], int num){
	int i;

	for(i=0; i < N; i++){
		if(arr[i] == num){
			return 1;
		}
	}
	return 0;
}

void printArray(int arr[N]){
	int col;

	for(col=0; col < N; col++){
		printf("%d ",arr[col]);
	}
	/*printf("\n");*/
}

int delimitedRandom(int min, int max){
   return (rand()%(max-min+1))+min;
}