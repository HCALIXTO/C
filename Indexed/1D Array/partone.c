#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 4
#define MIN 0
#define MAX 9
#define NREP 1000

int delimitedRandom(int min, int max);

void printBoard(int arr[N]);

void setArrayRand(int arr[N]);

int isAscending(int arr[N]);

int main(void){
	int arr[N] = {0};
	int count;

	srand(time(NULL));/*seed the random with time, must do once in main*/

	for(count = 0; count < NREP; count++){
		setArrayRand(arr);
		printBoard(arr);
		isAscending(arr);
	}

	return 0;
}

void setArrayRand(int arr[N]){
	int i = 0;

	while(i < N){
		arr[i] = delimitedRandom(MIN, MAX);
		i++;
	}
}

int isAscending(int arr[N]){
	int col, prev = 0;

	for(col=0; col < N; col++){
		if(prev > arr[col]){
			printf("  Not Ascending. \n");
			return 0;
		}
		prev = arr[col];
	}

	printf("  Ascending. \n");
	return 1;
}

void printBoard(int arr[N]){
	int col;

	for(col=0; col < N; col++){
		printf("%d",arr[col]);
	}
	/*printf("\n");*/
}

int delimitedRandom(int min, int max){
   return (rand()%(max-min+1))+min;
}