#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define CSEC (double)(CLOCKS_PER_SEC)
#define NUMWORDS 128773
#define WORDLEN 11
#define FILENAME "eowl_shuffle.txt"

void add(char *orded[NUMWORDS], char *word, int i);

int binarySearch(int len, char *orded[NUMWORDS], char *word);

void insertionSort(char *orded[NUMWORDS], char *word);

int main(void) {
   clock_t c1, c2;
   FILE *f;
   char words[NUMWORDS][WORDLEN];
   char *orded[NUMWORDS] = {NULL};/*orded is an array of pointers to char*/
   char search[WORDLEN];
   int numWords = 0;
   int i;

   c1 = clock();

   f = fopen(FILENAME, "r");
   if(f == NULL){
      printf("Unable to open file.\n");
      exit(1);
   }
   while(fscanf(f, "%s", words[numWords]) > 0){
      insertionSort(orded, words[numWords]);
      numWords++;
   }

   c2 = clock();
   printf("%f\n", (double)(c2-c1)/CSEC);

   for(i=0; i<numWords; i++){
      printf("%s\n", orded[i]);
   }

   printf("Please input a word to check if it is in the list:\n");
   if(scanf("%s", search) == 1){
     if(binarySearch(NUMWORDS ,orded, search)){
       printf("It is in the list.\n");
     }else{
       printf("It is NOT in the list.\n");
     }
   }

   return 0;
}

int binarySearch(int len, char *orded[NUMWORDS], char *word){
  int left, middle, right;
  left = 0;
  right = len-1;
  while (left <= right) {
    middle = (right+left)/2;
    if(strcmp(orded[middle], word) == 0){
      return middle;
    }else{
      if(strcmp(orded[middle], word) < 0){
        left = middle +1;
      }else{
        right = middle-1;
      }
    }
  }
  return 0;
}
void insertionSort(char *orded[NUMWORDS], char *word){
   int i = 0, end = 1;
   while(i < NUMWORDS && end){
     /*If i is NULL it is the end of the actual list, so I add the word to i*/
      if(orded[i] != NULL){
         if(strcmp(orded[i], word) > 0){
            /*printf("Adding word %s\n", word);*/
            add(orded, word, i);
            end = 0;
         }
      }else{
         /*printf("Found a NULL %s\n", word);*/
         orded[i] = word;
         end = 0;
      }
      i++;
   }
}

void add(char *orded[NUMWORDS], char *word, int i){
   char *prev;
   do{
      prev = orded[i];
      orded[i] = word;
      word = prev;
      i++;
   }while (i < NUMWORDS);
}
