#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "LList.h"

#define CSEC (double)(CLOCKS_PER_SEC)
#define NUMWORDS 128773
#define WORDLEN 11
#define FILENAME "eowl_shuffle.txt"

int main(void) {
   clock_t c1, c2;
   FILE *f;
   char word[WORDLEN], search[WORDLEN];
   Node *start;

   c1 = clock();
   start = AllocateNode("\0");/*Create a mock first item*/
   f = fopen(FILENAME, "r");
   if(f == NULL){
      printf("Unable to open file.\n");
      exit(1);
   }
   while(fscanf(f, "%s", word) > 0){
      insertionSort(start, word);
   }
   start = start->next;/*remove the mock first item*/

   c2 = clock();
   printf("%f\n", (double)(c2-c1)/CSEC);

   PrintList(start);

   printf("Please input a word to check if it is in the list:\n");
   if(scanf("%s", search) == 1){
     if(InList(start, search)){
       printf("It is in the list.\n");
     }else{
       printf("It is NOT in the list.\n");
     }
   }

   return 0;
}
